from django.db import models


class Shortcut(models.Model):
    title = models.CharField(max_length=100)

    description = models.TextField(blank=True)

    url = models.URLField()

    class Meta:
        ordering = ("title",)

    def __str__(self):
        return self.title


class HeaderShortcut(models.Model):
    icon = models.CharField(
        max_length=50, help_text="Must be a valid FontAwesome class name"
    )
    title = models.CharField(max_length=100)
    url = models.URLField()
    order = models.SmallIntegerField(blank=True, unique=True)

    class Meta:
        ordering = ("order",)

    def get_next_order(self):
        last_shortcut = HeaderShortcut.objects.all().order_by("order").first()
        if not last_shortcut:
            return 0
        return last_shortcut.order + 1

    def save(self, *args, **kwargs):
        if self.order is None:
            self.order = self.get_next_order()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title
