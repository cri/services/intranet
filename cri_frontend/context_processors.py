from django.conf import settings

from cri_models import meili
from cri_models.models import CRIUser, CRIGroup
from . import models


def search(request):
    if request.user.is_anonymous:
        # Avoid key generation for unauthenticated users
        return {}

    indices = [
        (CRIUser.MeiliMeta.index_name, "Users"),
        (CRIGroup.MeiliMeta.index_name, "Groups"),
    ]

    return {
        "meili_uri": settings.MEILISEARCH.get("PUBLIC_SERVER_URI"),
        "meili_apikey": meili.get_user_search_key(request.user),
        "search_indices": indices,
    }


def header_shortcuts(request):
    shortcuts = models.HeaderShortcut.objects.all()
    return {"header_shortcuts": shortcuts}


def custom_site_name(request):
    return {"custom_site_name": settings.SITE_NAME}
