from django.apps import AppConfig


class CriFrontendConfig(AppConfig):
    name = "cri_frontend"
