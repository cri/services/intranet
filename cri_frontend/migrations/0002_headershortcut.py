# Generated by Django 3.2.16 on 2022-11-24 16:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("cri_frontend", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="HeaderShortcut",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "icon",
                    models.CharField(
                        help_text="Must be a valid FontAwesome class name",
                        max_length=50,
                    ),
                ),
                ("title", models.CharField(max_length=100)),
                ("url", models.URLField()),
                ("order", models.SmallIntegerField(blank=True, unique=True)),
            ],
            options={
                "ordering": ("order",),
            },
        ),
    ]
