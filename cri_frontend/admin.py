from django.contrib import admin

from . import models


@admin.register(models.Shortcut)
class ShortcutAdmin(admin.ModelAdmin):
    list_display = ("title", "url")
    list_didplay_links = list_display
    search_fields = ("title", "url", "description")


@admin.register(models.HeaderShortcut)
class HeaderShortcutAdmin(admin.ModelAdmin):
    list_display = ("order", "title", "icon", "url")
    list_display_links = ("title", "icon", "url")
    list_editable = ("order",)
    search_fields = ("title", "url")
