{
  inputs = {
    nixpkgs.url = "github:g00pix/nixpkgs/bump-dlib";
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    futils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, poetry2nix, futils } @ inputs:
    let
      inherit (nixpkgs) lib;
      inherit (futils.lib) eachDefaultSystem defaultSystems;

      nixpkgsFor = lib.genAttrs defaultSystems (system: import nixpkgs {
        inherit system;
        overlays = [ poetry2nix.overlays.default ];
      });
    in
    (eachDefaultSystem (system:
      let
        pkgs = nixpkgsFor.${system};
      in
      {
        devShells.default = pkgs.mkShell {
          buildInputs = with pkgs; [
            (pkgs.poetry2nix.mkPoetryEnv {
              projectDir = self;
              python = pkgs.python311;

              overrides = pkgs.poetry2nix.overrides.withDefaults (self: super: {

                # Custom EPITA packages
                python-kadmin-epita = super.python-kadmin-epita.overridePythonAttrs (old: {
                  buildInputs = (old.buildInputs or [ ]) ++ [ pkgs.libkrb5 ];
                  nativeBuildInputs = (old.nativeBuildInputs or [ ]) ++ [ self.setuptools self.poetry ];
                });
                epita-env = super.epita-env.overridePythonAttrs (old: {
                  nativeBuildInputs = (old.nativeBuildInputs or [ ]) ++ [ self.poetry ];
                });
                face-crop = super.face-crop.overridePythonAttrs (old: {
                  nativeBuildInputs = (old.nativeBuildInputs or [ ]) ++ [ self.poetry ];
                });

                # Missing packages from poetry2nix overrides
                # Here until commited upstream
                django-oidc-provider = super.django-oidc-provider.overridePythonAttrs (old: {
                  nativeBuildInputs = (old.nativeBuildInputs or [ ]) ++ [ self.setuptools ];
                });
                django-ldapdb = super.django-ldapdb.overridePythonAttrs (old: {
                  nativeBuildInputs = (old.nativeBuildInputs or [ ]) ++ [ self.setuptools ];
                });
                collectfast = super.collectfast.overridePythonAttrs (old: {
                  nativeBuildInputs = (old.nativeBuildInputs or [ ]) ++ [ self.setuptools ];
                });
                django-mail-panel = super.django-mail-panel.overridePythonAttrs (old: {
                  nativeBuildInputs = (old.nativeBuildInputs or [ ]) ++ [ self.poetry ];
                });
                django-rest-serializer-field-permissions = super.django-rest-serializer-field-permissions.overridePythonAttrs (old: {
                  nativeBuildInputs = (old.nativeBuildInputs or [ ]) ++ [ self.setuptools ];
                });
                django-import-export = super.django-import-export.overridePythonAttrs (old: {
                  nativeBuildInputs = (old.nativeBuildInputs or [ ]) ++ [ self.setuptools ];
                });
                setoptconf-tmp = super.setoptconf-tmp.overridePythonAttrs (old: {
                  nativeBuildInputs = (old.nativeBuildInputs or [ ]) ++ [ self.setuptools ];
                });
                crispy-bootstrap4 = super.crispy-bootstrap4.overridePythonAttrs (old: {
                  nativeBuildInputs = (old.nativeBuildInputs or [ ]) ++ [ self.setuptools ];
                });
              });
            })
            git
            nixpkgs-fmt
          ];
        };
      }
    ));
}
