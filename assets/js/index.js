import $ from "expose-loader?exposes=$,jQuery!jquery";
import { MeiliSearch } from 'expose-loader?exposes=MeiliSearch!meilisearch'

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

import '@fortawesome/fontawesome-free/js/fontawesome'
import '@fortawesome/fontawesome-free/js/solid'
import '@fortawesome/fontawesome-free/js/regular'
import '@fortawesome/fontawesome-free/js/brands'

import '../css/base.css'
