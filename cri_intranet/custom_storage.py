from django.conf import settings
from storages.backends.s3 import S3Storage


class PublicS3Storage(S3Storage):
    """
    This class is a S3 django-storages class. It adds a custom domain setting to
    the default S3 storage class. This is useful because django-storages does
    not sign object URLs when a custom_domain is set, thus saving us a lot of
    overhead. We don't want that to be the default behavior so we add a custom
    setting name.

    It is necessary to add the AWS_PUBLIC_CUSTOM_DOMAIN setting.
    """

    custom_domain = settings.AWS_PUBLIC_CUSTOM_DOMAIN
    object_parameters = settings.AWS_PUBLIC_OBJECT_PARAMETERS


class PhotoStorage(S3Storage):
    bucket_name = settings.AWS_STORAGE_PHOTOS_BUCKET_NAME
