from django.contrib.auth.models import Permission
from django.urls import reverse

from rest_framework import test, status

from cri_models import models as cri_models


class TestCRIUserMembership(test.APITestCase):
    fixtures = ("tests/groups.yaml",)

    @classmethod
    def setUpTestData(cls):
        for index, name in enumerate(("group1", "group2")):
            cri_models.CRIGroup.objects.get_or_create(slug=name)

        for index, name in enumerate(("adminuser", "user1", "user2")):
            user, _ = cri_models.CRIUser.objects.get_or_create(
                username=name,
                email="{name}@example.org",
                uid=42 + index,
            )

            if user.username == "adminuser":
                user.user_permissions.add(
                    *Permission.objects.filter(
                        content_type__app_label="cri_models",
                        content_type__model="crimembership",
                    )
                )

            if user.username == "user1":
                cri_models.CRIMembership.objects.get_or_create(
                    user=user, group=cri_models.CRIGroup.objects.get(slug="group1")
                )

            if user.username == "user2":
                cri_models.CRIMembership.objects.get_or_create(
                    user=user, group=cri_models.CRIGroup.objects.get(slug="group2")
                )

    def test_memberships_anon_list(self):
        url = reverse("criuser-memberships-list", kwargs={"login": "user1"})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_memberships_auth_list(self):
        url = reverse("criuser-memberships-list", kwargs={"login": "user2"})
        user1 = cri_models.CRIUser.objects.get(username="user1")
        self.client.force_authenticate(user=user1)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.json()
        self.assertEqual(data.get("count"), 1)
        membership = data.get("results", [])[0]
        self.assertEqual(membership["group"], "group2")

    def test_memberships_unauthorized_create(self):
        url = reverse("criuser-memberships-list", kwargs={"login": "user2"})
        user1 = cri_models.CRIUser.objects.get(username="user1")
        self.client.force_authenticate(user=user1)
        response = self.client.post(url, {"group": "group1"})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_memberships_authorized_create(self):
        url = reverse("criuser-memberships-list", kwargs={"login": "user2"})
        admin = cri_models.CRIUser.objects.get(username="adminuser")
        self.client.force_authenticate(user=admin)
        response = self.client.post(url, {"group": "group1"})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        data = response.json()
        group = data.get("group")
        self.assertEqual(group, "group1")

    def test_memberships_authorized_patch(self):
        user1 = cri_models.CRIUser.objects.get(username="user1")
        membership = cri_models.CRIMembership.objects.filter(user=user1).first()
        url = reverse(
            "criuser-memberships-detail", kwargs={"login": "user1", "pk": membership.pk}
        )
        admin = cri_models.CRIUser.objects.get(username="adminuser")
        self.client.force_authenticate(user=admin)
        response = self.client.patch(url, data={"graduation_year": 4242})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        memb = response.json()
        self.assertEqual(memb["graduation_year"], 4242)
