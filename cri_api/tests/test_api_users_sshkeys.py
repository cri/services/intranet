from django.urls import reverse

from rest_framework import test, status

from cri_models import models as cri_models


class TestCRIUser(test.APITestCase):
    fixtures = ("tests/groups.yaml",)

    SSH_KEYS = (
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOrvjSGhfjaWn/ANO1uQENVx+XW78XVSlvR64m7iYf8+ comment",  # noqa: E501
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPhWx8VR9STySYloHUwpCTH8kayMmzVqgs5EGHb0ar0Y comment",  # noqa: E501
    )

    NEW_SSH_KEY = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFa/xZaC8jIc7H7nBtRMMmVKgGdY85iGqzCWpzZZKI/0"  # noqa: E501

    @classmethod
    def setUpTestData(cls):
        for index, name in enumerate(("user1", "user2")):
            user, _ = cri_models.CRIUser.objects.get_or_create(
                username=name,
                email="{name}@example.org",
                uid=42 + index,
            )
            cri_models.SSHPublicKey.objects.get_or_create(
                user=user,
                title=f"title",
                key=cls.SSH_KEYS[index],
            )

    def test_sshkeys_anonymous_no_title_no_comment(self):
        url = reverse("criuser-sshkeys-list", kwargs={"login": "user1"})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.json()
        self.assertEqual(data.get("count"), 1)
        key = data.get("results", [])[0]
        self.assertNotIn("comment", key.get("key"))
        self.assertNotIn("title", key)

    def test_sshkeys_authenticated_title_and_comment(self):
        url = reverse("criuser-sshkeys-list", kwargs={"login": "user1"})
        user1 = cri_models.CRIUser.objects.get(username="user1")
        self.client.force_authenticate(user=user1)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.json()
        self.assertEqual(data.get("count"), 1)
        key = data.get("results", [])[0]
        self.assertEqual(key["title"], "title")
        self.assertIn("comment", key.get("key"))

    def test_sshkeys_create_self_key(self):
        user1 = cri_models.CRIUser.objects.get(username="user1")
        url = reverse("criuser-sshkeys-list", kwargs={"login": "user1"})
        self.client.force_authenticate(user=user1)
        response = self.client.post(url, {"key": self.NEW_SSH_KEY, "title": "test"})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(user1.sshpublickey_set.count(), 2)
        key = cri_models.SSHPublicKey.objects.get(pk=response.json()["id"])
        self.assertEqual(key.title, "test")
        self.assertEqual(str(key.key), self.NEW_SSH_KEY)

    def test_sshkeys_create_other_key(self):
        user1 = cri_models.CRIUser.objects.get(username="user1")
        url = reverse("criuser-sshkeys-list", kwargs={"login": "user2"})
        self.client.force_authenticate(user=user1)
        response = self.client.post(url, {"key": self.NEW_SSH_KEY, "title": "test"})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(user1.sshpublickey_set.count(), 1)

    def test_sshkeys_create_anonymous_key(self):
        url = reverse("criuser-sshkeys-list", kwargs={"login": "user2"})
        response = self.client.post(url, {"key": self.NEW_SSH_KEY, "title": "test"})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_sshkeys_delete_self_key(self):
        user1 = cri_models.CRIUser.objects.get(username="user1")
        key1 = user1.sshpublickey_set.first()
        url = reverse(
            "criuser-sshkeys-detail", kwargs={"login": "user1", "pk": key1.pk}
        )
        self.client.force_authenticate(user=user1)
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        with self.assertRaises(cri_models.SSHPublicKey.DoesNotExist):
            key1.refresh_from_db()

    def test_sshkeys_delete_other_key(self):
        user1 = cri_models.CRIUser.objects.get(username="user1")
        user2 = cri_models.CRIUser.objects.get(username="user2")
        key2 = user2.sshpublickey_set.first()
        url = reverse(
            "criuser-sshkeys-detail", kwargs={"login": "user2", "pk": key2.pk}
        )
        self.client.force_authenticate(user=user1)
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        key2.refresh_from_db()

    def test_sshkeys_delete_key_anonymous(self):
        user1 = cri_models.CRIUser.objects.get(username="user1")
        key1 = user1.sshpublickey_set.first()
        url = reverse(
            "criuser-sshkeys-detail", kwargs={"login": "user1", "pk": key1.pk}
        )
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        key1.refresh_from_db()

    def test_user_search_by_ssh_key(self):
        user1 = cri_models.CRIUser.objects.get(username="user1")
        url = reverse("criuser-search")
        self.client.force_authenticate(user=user1)
        response = self.client.get(
            url,
            {"ssh_key": self.SSH_KEYS[0]},
            format="multipart",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.json()
        self.assertEqual(data["count"], 1)
        self.assertEqual(data["results"][0]["login"], "user1")
