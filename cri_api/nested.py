from rest_framework import routers as drf_routers
from rest_framework_nested import routers


class NoLookUpMixin:
    def __init__(self, *args, **kwargs):
        self._nest_prefix = kwargs.get("lookup", None)
        super().__init__(*args, **kwargs)

    @property
    def nest_prefix(self):
        if self._nest_prefix:
            return self._nest_prefix + "_"
        return ""

    @nest_prefix.setter
    def nest_prefix(self, _value):
        return

    def check_valid_name(self, value):
        if self.nest_prefix == "":
            return True
        return super().check_valid_name(value)


class NestedSimpleRouter(NoLookUpMixin, routers.NestedMixin, drf_routers.SimpleRouter):
    pass


class NestedDefaultRouter(
    NoLookUpMixin, routers.NestedMixin, drf_routers.DefaultRouter
):
    pass
