from django.core import validators
from django.shortcuts import get_object_or_404
from django.db.models import Q

from rest_framework import (
    viewsets,
    permissions,
    generics,
    mixins,
    status,
    serializers as drf_serializers,
)
from rest_framework.decorators import action
from rest_framework.response import Response
from drf_yasg.utils import swagger_auto_schema, no_body

from cri_models import models
from cri_cards import models as cri_cards_models

from . import filters, serializers, permissions as cri_api_permissions


class DynamicSerializerClassMixin:
    def get_serializer_class(self):
        """
        A class which inherits this mixins should have variable
        `serializer_action_classes`.
        Look for serializer class in self.serializer_action_classes, which
        should be a dict mapping action name (key) to serializer class (value),
        i.e.:
        class SampleViewSet(viewsets.ViewSet):
            serializer_class = DocumentSerializer
            serializer_action_classes = {
               'upload': UploadDocumentSerializer,
               'download': DownloadDocumentSerializer,
            }
            @action
            def upload:
                ...
        If there's no entry for that action then just fallback to the regular
        get_serializer_class lookup: self.serializer_class, DefaultSerializer.
        """
        try:
            return self.serializer_action_classes[self.action]
        except (KeyError, AttributeError):
            return super().get_serializer_class()


class CRIGroupHistoryView(generics.ListAPIView):
    queryset = models.CRIGroup.objects.all()
    real_queryset = models.CRIComputedMembership.objects.all()
    serializer_class = serializers.CRIComputedMembershipSerializer
    lookup_field = "slug"
    lookup_url_kwarg = "slug"

    def get_queryset(self):
        if not self.kwargs:
            return self.queryset
        return self.real_queryset

    def get_object(self):
        kwargs = {self.lookup_field: self.kwargs[self.lookup_url_kwarg]}
        user = self.request.user
        if user.has_perm("cri_models.view_crigroup"):
            group_filter = []
        else:
            group_filter = [
                Q(private=False) | Q(pk__in=[g.pk for g in user.get_groups()])
            ]
        return get_object_or_404(models.CRIGroup, *group_filter, **kwargs)

    def filter_queryset(self, queryset):
        return queryset.filter(group=self.get_object())

    def get_serializer(self, *args, **kwargs):
        kwargs["context"] = self.get_serializer_context()
        kwargs.setdefault("excluded_fields", []).append("group")
        return self.get_serializer_class()(*args, **kwargs)


class CRIGroupMembersView(CRIGroupHistoryView):
    real_queryset = models.CRIUser.objects.all()
    serializer_class = serializers.SimpleCRIUserSerializer

    def filter_queryset(self, queryset):
        group = self.get_object()
        return queryset.filter(pk__in=[u.pk for u in group.get_members()])

    def get_serializer(self, *args, **kwargs):
        kwargs["context"] = self.get_serializer_context()
        return self.get_serializer_class()(*args, **kwargs)


class CampusViewSet(viewsets.ModelViewSet):
    queryset = models.Campus.objects.all()
    serializer_class = serializers.CampusSerializer
    lookup_field = "group__slug"
    lookup_url_kwarg = "slug"


class CRIGroupViewSet(viewsets.ModelViewSet):
    queryset = models.CRIGroup.objects.all()
    serializer_class = serializers.CRIGroupSerializer
    filterset_fields = ("kind", "private")
    lookup_field = "slug"

    def filter_queryset(self, queryset):
        queryset = super().filter_queryset(queryset)
        user = self.request.user
        if user.has_perm("cri_models.view_crigroup"):
            return queryset

        return queryset.filter(
            Q(private=False) | Q(pk__in=[g.pk for g in user.get_groups()])
        )


class CRIUserViewSet(DynamicSerializerClassMixin, viewsets.ModelViewSet):
    queryset = models.CRIUser.objects.all()
    serializer_class = serializers.CRIUserSerializer
    serializer_action_classes = {"create": serializers.CRIUserCreateSerializer}
    filterset_class = None
    lookup_field = "username"
    lookup_url_kwarg = "login"
    lookup_value_regex = "[^/]+"

    @action(
        detail=False,
        methods=["get"],
        permission_classes=[permissions.IsAuthenticated],
        serializer_class=serializers.ProfileSerializer,
        filter_backends=(),
        pagination_class=None,
    )
    def me(self, request, **_kwargs):
        serializer = serializers.ProfileSerializer(
            request.user, context={"request": request}
        )
        return Response(serializer.data)

    @action(
        detail=False,
        methods=["get"],
        filterset_class=filters.CRIUserFilter,
    )
    def search(self, request, **_kwargs):
        filtered_qs = self.filterset_class(  # pylint: disable=not-callable
            request.GET, queryset=models.CRIUser.objects.all()
        ).qs

        page = self.paginate_queryset(filtered_qs)
        if page is not None:
            serializer = serializers.CRIUserSerializer(
                page, context={"request": request}, many=True
            )
            return self.get_paginated_response(serializer.data)
        serializer = serializers.CRIUserSerializer(
            filtered_qs, context={"request": request}, many=True
        )
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        # Function called when a user sends a POST request on the route.

        # This override is a huge hack because DRF doesn't care when overriding
        # the required flag on required models fields. Here, we want to allow
        # not specifying a new user's uid so it is computed by the server.

        if request.data.get("uid") is None:
            primary_group_slug = request.data.get("primary_group")
            if primary_group_slug is not None:
                primary_group = models.CRIGroup.objects.filter(
                    slug=primary_group_slug
                ).first()
                if primary_group is not None and primary_group.gid is not None:
                    request.data.update(
                        {"uid": models.CRIUser.get_next_available_uid(primary_group)}
                    )
        return super().create(request, *args, **kwargs)


class SSHPublicKeyViewSet(
    cri_api_permissions.SwaggerPermissionOverrideMixin, viewsets.ModelViewSet
):
    queryset = models.SSHPublicKey.objects.all()
    serializer_class = serializers.SSHPublicKeySerializer
    permission_classes = [cri_api_permissions.DjangoSelfObjectOrAnonReadOnly]
    swagger_permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        if "login" in self.kwargs:
            return self.queryset.filter(
                user__username=self.kwargs["login"],
            )
        return self.queryset

    def get_serializer(self, *args, **kwargs):
        kwargs["context"] = self.get_serializer_context()
        if self.request.user.is_anonymous:
            kwargs["strip_comment"] = True
        elif "login" in self.kwargs:
            kwargs["user"] = get_object_or_404(
                models.CRIUser, username=self.kwargs["login"]
            )
        kwargs.setdefault("excluded_fields", []).append("user")
        return self.get_serializer_class()(*args, **kwargs)


class MailAliasViewSetMixin:
    queryset = models.MailAlias.objects.all()
    permission_classes = [permissions.DjangoModelPermissions]
    http_method_names = ["put", "delete", "head", "options", "trace"]
    serializer_class = drf_serializers.Serializer
    lookup_field = "email"
    lookup_url_kwarg = "email"
    lookup_value_regex = "[^/]+"

    def dispatch(self, request, *args, **kwargs):
        self.obj = get_object_or_404(
            self.model, **{self.model_lookup_field: kwargs[self.model_lookup_url_kwarg]}
        )
        return super().dispatch(request, *args, **kwargs)

    def _validate_email(self, email):
        try:
            validators.validate_email(email)
        except validators.ValidationError:
            raise drf_serializers.ValidationError(
                {self.lookup_url_kwarg: "This field must be a valid email address."}
            )

    @swagger_auto_schema(
        request_body=no_body,
        responses={
            201: "Mail alias added to parent object",
            204: "Mail alias already associated with parent object",
        },
    )
    def update(self, request, *_args, **kwargs):
        self._validate_email(kwargs["email"])
        instance, _ = self.queryset.get_or_create(email=kwargs["email"])
        new = instance not in self.obj.mail_aliases.all()
        self.obj.mail_aliases.add(instance)

        if new:
            return Response(status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def destroy(self, request, *_args, **kwargs):
        self._validate_email(kwargs["email"])
        instance = get_object_or_404(
            models.MailAlias, **{self.lookup_field: kwargs[self.lookup_url_kwarg]}
        )
        if instance not in self.obj.mail_aliases.all():
            return Response(status=status.HTTP_404_NOT_FOUND)
        self.obj.mail_aliases.remove(instance)

        if instance.crigroup_set.count() == instance.criuser_set.count() == 0:
            instance.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)


class UserMailAliasViewSet(
    MailAliasViewSetMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    model = models.CRIUser
    model_lookup_field = "username"
    model_lookup_url_kwarg = "login"


class GroupMailAliasViewSet(
    MailAliasViewSetMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    model = models.CRIGroup
    model_lookup_field = "slug"
    model_lookup_url_kwarg = "slug"


class CRICardsView(generics.CreateAPIView):
    queryset = cri_cards_models.Card.objects.all()
    serializer_class = serializers.CardGenerationSerializer
    permission_classes = [cri_api_permissions.CanIssueCards]


class UserMembershipViewSet(viewsets.ModelViewSet):
    queryset = models.CRIMembership.objects.all()
    serializer_class = serializers.CRIUserMembershipSerializer
    permission_classes = [permissions.DjangoModelPermissions]

    def get_queryset(self):
        if "login" in self.kwargs:
            return self.queryset.filter(
                user__username=self.kwargs["login"],
            )
        return self.queryset

    def get_serializer(self, *args, **kwargs):
        kwargs["context"] = self.get_serializer_context()
        if "login" in self.kwargs:
            kwargs["user"] = get_object_or_404(
                models.CRIUser, username=self.kwargs["login"]
            )
        return self.get_serializer_class()(*args, **kwargs)


class MembershipCreateView(generics.CreateAPIView):
    queryset = models.CRIMembership.objects.all()
    serializer_class = serializers.CreateCRIMembershipSerializer
    permission_classes = [permissions.DjangoModelPermissions]

    def get_serializer(self, *args, **kwargs):
        kwargs["context"] = self.get_serializer_context()
        if isinstance(kwargs.get("data", {}), list):
            kwargs["many"] = True
        return self.get_serializer_class()(*args, **kwargs)


class UserPhotoKindViewSet(viewsets.ModelViewSet):
    queryset = models.CRIPhotoKind.objects.all()
    serializer_class = serializers.CRIPhotoKindSerializer
    permission_classes = [permissions.IsAdminUser]


class UserPhotoViewSet(viewsets.ModelViewSet):
    queryset = models.CRIPhoto.objects.all()
    serializer_class = serializers.CRIPhotoSerializer
    permission_classes = [permissions.IsAdminUser]

    def get_queryset(self):
        if "login" in self.kwargs:
            return self.queryset.filter(
                user__username=self.kwargs["login"],
            )
        return self.queryset

    def get_serializer(self, *args, **kwargs):
        kwargs["context"] = self.get_serializer_context()
        if "login" in self.kwargs:
            kwargs["user"] = get_object_or_404(
                models.CRIUser, username=self.kwargs["login"]
            )
        return self.get_serializer_class()(*args, **kwargs)


class UserCreationRequestViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAdminUser]
    serializer_class = serializers.CRIUserCreationRequestSerializer
    queryset = models.CRIUserCreationRequest.objects.all().select_related(
        "primary_group", "scope", "campus"
    )
    filterset_fields = ("status", "scope")

    @action(
        detail=True,
        serializer_class=None,
        methods=["post"],
        permission_classes=[permissions.IsAdminUser],
    )
    def create_user(self, request, **_kwargs):
        userreq: models.CRIUserCreationRequest = self.get_object()
        userreq.create(request.user)
        serializer = serializers.CRIUserCreationRequestSerializer(userreq)
        return Response(serializer.data)
