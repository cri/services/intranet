from django.db import transaction

from rest_framework import serializers
from rest_framework_serializer_field_permissions.serializers import (
    FieldPermissionSerializerMixin,
)
from rest_framework_serializer_field_permissions import (
    fields,
    permissions as field_permissions,
)
from rest_framework_nested.serializers import NestedHyperlinkedModelSerializer

from cri_models import models
from .. import permissions

from .base import (
    FieldExclusionSerializerMixin,
    SimpleCRIGroupSerializer,
)
from .group import CRIComputedMembershipSerializer


class SSHPublicKeySerializer(
    FieldPermissionSerializerMixin,
    FieldExclusionSerializerMixin,
    NestedHyperlinkedModelSerializer,
):
    title = fields.CharField(
        required=False,
        permission_classes=(field_permissions.IsAuthenticated(),),
    )

    key_type = serializers.CharField(read_only=True)

    key_length = serializers.CharField(read_only=True)

    class Meta:
        model = models.SSHPublicKey
        fields = ("id", "title", "key", "key_type", "key_length")
        extra_kwargs = {"url": {"lookup_field": "id"}}

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user", None)
        self.strip_comment = kwargs.pop("strip_comment", False)
        super().__init__(*args, **kwargs)

    def to_representation(self, instance):
        r = super().to_representation(instance)
        if self.strip_comment:
            r["key"] = instance.key.key_without_comment
        return r

    def create(self, validated_data):
        validated_data["user"] = self.user
        return super().create(validated_data)


class ProfileSerializer(serializers.HyperlinkedModelSerializer):
    login = serializers.CharField(read_only=True, source="username")

    primary_group = SimpleCRIGroupSerializer(read_only=True)

    groups_history = CRIComputedMembershipSerializer(
        many=True,
        read_only=True,
        source="memberships",
        excluded_fields=["user"],
    )

    current_groups = SimpleCRIGroupSerializer(
        many=True, read_only=True, source="get_groups"
    )

    old_accounts = fields.ListField(
        read_only=True, child=fields.CharField(), source="get_old_accounts"
    )

    new_account = serializers.CharField(
        required=False,
        read_only=True,
        allow_null=True,
        source="get_new_account",
    )

    sshkeys = SSHPublicKeySerializer(
        many=True, read_only=True, source="sshpublickey_set"
    )

    mail_aliases = serializers.ListField(
        child=serializers.EmailField(),
        allow_empty=True,
        read_only=True,
        source="get_mail_aliases",
    )

    class Meta:
        model = models.CRIUser
        fields = (
            "url",
            "login",
            "uid",
            "primary_group",
            "first_name",
            "last_name",
            "legal_first_name",
            "legal_last_name",
            "email",
            "mail_aliases",
            "phone",
            "birthdate",
            "groups_history",
            "current_groups",
            "old_accounts",
            "new_account",
            "nickname",
            "callsign",
            "sshkeys",
        )
        extra_kwargs = {
            "url": {"lookup_field": "username", "lookup_url_kwarg": "login"},
            "uid": {"required": False},
        }


class CRIUserSerializer(FieldPermissionSerializerMixin, ProfileSerializer):
    old_accounts = fields.ListField(
        read_only=True,
        child=fields.CharField(),
        source="get_old_accounts",
        permission_classes=(permissions.CanViewRelatedAccounts(),),
    )

    new_account = fields.CharField(
        required=False,
        read_only=True,
        allow_null=True,
        source="get_new_account",
        permission_classes=(permissions.CanViewRelatedAccounts(),),
    )

    legal_first_name = fields.CharField(
        required=False,
        permission_classes=(permissions.CanViewLegalIdentity(),),
    )

    legal_last_name = fields.CharField(
        required=False,
        permission_classes=(permissions.CanViewLegalIdentity(),),
    )

    phone = fields.CharField(
        required=False, permission_classes=(permissions.CanViewPhone(),)
    )

    birthdate = fields.CharField(
        required=False, permission_classes=(permissions.CanViewBirthdate(),)
    )


class NestedCRIMembershipCreateSerializer(serializers.ModelSerializer):
    group = serializers.SlugRelatedField(
        slug_field="slug", queryset=models.CRIGroup.objects.all()
    )

    class Meta:
        model = models.CRIMembership
        fields = ("group", "begin_at", "end_at", "graduation_year")


class NestedKerberosPrincipalSerializer(serializers.ModelSerializer):
    is_primary = fields.BooleanField(write_only=True, default=False)

    class Meta:
        model = models.KerberosPrincipal
        fields = ("principal", "is_primary", "out_of_date")


class CRIUserCreateSerializer(serializers.ModelSerializer):
    primary_group = serializers.CharField()

    login = serializers.CharField(source="username")

    memberships = NestedCRIMembershipCreateSerializer(many=True)

    kerberos_principals = NestedKerberosPrincipalSerializer(
        many=True, source="kerberosprincipal_set"
    )

    class Meta:
        model = models.CRIUser
        fields = (
            "login",
            "uid",
            "primary_group",
            "first_name",
            "last_name",
            "legal_first_name",
            "legal_last_name",
            "email",
            "phone",
            "birthdate",
            "memberships",
            "kerberos_principals",
            "nickname",
            "callsign",
        )

    def validate(self, attrs):
        principals = attrs.get("kerberosprincipal_set", [])
        if len([p for p in principals if p.get("is_primary", False)]) > 1:
            raise serializers.ValidationError(
                "Only one Kerberos principal can be set as primary principal"
            )

        return super().validate(attrs)

    @transaction.atomic()
    def create(self, validated_data):
        primary_group = models.CRIGroup.objects.get(
            slug=validated_data.pop("primary_group")
        )
        validated_data["primary_group"] = primary_group
        memberships = validated_data.pop("memberships", [])
        kerberos_principals = validated_data.pop("kerberosprincipal_set", [])
        instance = super().create(validated_data)
        for membership in memberships:
            membership["user"] = instance
            NestedCRIMembershipCreateSerializer(membership).create(membership)
        for principal in kerberos_principals:
            principal["user"] = instance
            is_primary = principal.pop("is_primary", False)
            principal_instance = NestedKerberosPrincipalSerializer(principal).create(
                principal
            )
            if is_primary:
                instance.primary_principal = principal_instance
                instance.save()
        instance.sync()
        return instance


class CRIUserSearchSerializer(serializers.Serializer):
    logins = fields.ListField(write_only=True, child=fields.CharField())

    uids = fields.ListField(write_only=True, child=fields.IntegerField())


class CRIMembershipBulkSerializer(serializers.ListSerializer):
    def create(self, validated_data):
        result = [models.CRIMembership(**attrs) for attrs in validated_data]
        models.CRIMembership.objects.bulk_create(result)
        models.CRIComputedMembership.compute_entries()
        return result


class CreateCRIMembershipSerializer(serializers.ModelSerializer):
    group = serializers.SlugRelatedField(
        slug_field="slug", queryset=models.CRIGroup.objects.all()
    )

    class Meta:
        model = models.CRIMembership
        fields = (
            "id",
            "user",
            "group",
            "begin_at",
            "end_at",
            "graduation_year",
        )
        list_serializer_class = CRIMembershipBulkSerializer


class CRIUserMembershipSerializer(serializers.ModelSerializer):
    group = serializers.SlugRelatedField(
        slug_field="slug", queryset=models.CRIGroup.objects.all()
    )

    is_current = fields.BooleanField(read_only=True)

    class Meta:
        model = models.CRIMembership
        fields = (
            "id",
            "group",
            "begin_at",
            "end_at",
            "graduation_year",
            "is_current",
        )
        extra_kwargs = {"url": {"lookup_field": "id"}}

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user", None)
        super().__init__(*args, **kwargs)

    def create(self, validated_data):
        validated_data["user"] = self.user
        return super().create(validated_data)


class CRIPhotoKindSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CRIPhotoKind
        fields = [
            "name",
            "priority",
        ]


class CRIPhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CRIPhoto
        fields = [
            "uuid",
            "user",
            "kind",
            "image",
            "image_thumb",
            "image_square",
            "priority",
        ]

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user", None)
        super().__init__(*args, **kwargs)

    def create(self, validated_data):
        validated_data["user"] = self.user
        return super().create(validated_data)
