from rest_framework import serializers
from rest_framework_serializer_field_permissions.serializers import (
    FieldPermissionSerializerMixin,
)
from rest_framework_serializer_field_permissions import fields

from cri_models import models
from .. import permissions


class FieldExclusionSerializerMixin:
    def __init__(self, *args, **kwargs):
        self.excluded_fields = kwargs.pop("excluded_fields", [])
        super().__init__(*args, **kwargs)

    def to_representation(self, instance):
        r = super().to_representation(instance)
        return {k: v for k, v in r.items() if k not in self.excluded_fields}


class SimpleCRIUserSerializer(
    FieldPermissionSerializerMixin, serializers.HyperlinkedModelSerializer
):
    login = serializers.CharField(read_only=True, source="username")

    old_accounts = fields.ListField(
        read_only=True,
        source="get_old_accounts",
        child=fields.CharField(),
        permission_classes=(permissions.CanViewRelatedAccounts(),),
    )

    new_account = fields.CharField(
        read_only=True,
        allow_null=True,
        source="get_new_account",
        permission_classes=(permissions.CanViewRelatedAccounts(),),
    )

    class Meta:
        model = models.CRIUser
        fields = ("url", "login", "old_accounts", "new_account")
        extra_kwargs = {
            "url": {"lookup_field": "username", "lookup_url_kwarg": "login"}
        }


class SimpleCRIGroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.CRIGroup
        fields = ("url", "slug")
        extra_kwargs = {"url": {"lookup_field": "slug"}}
