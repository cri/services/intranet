from rest_framework import serializers

from cri_models import models


class CRIUserCreationRequestSerializer(serializers.ModelSerializer):
    campus = serializers.SlugRelatedField(
        slug_field="slug",
        queryset=models.CRIGroup.objects.filter(kind="campus"),
        required=False,
    )
    primary_group = serializers.SlugRelatedField(
        slug_field="slug",
        queryset=models.CRIGroup.objects.filter(gid__isnull=False),
        required=False,
    )
    scope = serializers.StringRelatedField()

    class Meta:
        model = models.CRIUserCreationRequest
        fields = [
            "id",
            "username",
            "email",
            "external_email",
            "first_name",
            "last_name",
            "primary_group",
            "birthdate",
            "phone",
            "scope",
            "campus",
            "status",
        ]
