from rest_framework import serializers

from cri_models import models


class MailAliasSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.MailAlias
        fields = ("email",)
