from rest_framework import serializers, fields

from cri_models import models

from .base import (
    FieldExclusionSerializerMixin,
    SimpleCRIUserSerializer,
    SimpleCRIGroupSerializer,
)


class CRIGroupSerializer(serializers.HyperlinkedModelSerializer):
    members_url = serializers.HyperlinkedIdentityField(
        read_only=True, view_name="crigroup-members", lookup_field="slug"
    )

    history_url = serializers.HyperlinkedIdentityField(
        read_only=True, view_name="crigroup-history", lookup_field="slug"
    )

    managers = SimpleCRIUserSerializer(read_only=True, many=True, source="get_managers")

    mail_aliases = serializers.ListField(
        child=serializers.EmailField(),
        allow_empty=True,
        read_only=True,
        source="get_mail_aliases",
    )

    class Meta:
        model = models.CRIGroup
        fields = (
            "url",
            "slug",
            "gid",
            "name",
            "kind",
            "members_url",
            "history_url",
            "mail_aliases",
            "managers",
            "private",
        )
        extra_kwargs = {"url": {"lookup_field": "slug"}}


class CRIComputedMembershipSerializer(
    FieldExclusionSerializerMixin,
    serializers.ModelSerializer,
):
    user = SimpleCRIUserSerializer(read_only=True)

    group = SimpleCRIGroupSerializer(read_only=True)

    is_current = fields.BooleanField(read_only=True)

    class Meta:
        model = models.CRIComputedMembership
        fields = (
            "group",
            "user",
            "begin_at",
            "end_at",
            "graduation_year",
            "is_current",
        )
