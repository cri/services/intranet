from rest_framework import serializers

from cri_models import models

from .group import SimpleCRIGroupSerializer


class CampusSerializer(serializers.HyperlinkedModelSerializer):
    name = serializers.CharField(source="group.name", read_only=True)

    group = SimpleCRIGroupSerializer(read_only=True)

    class Meta:
        model = models.Campus
        fields = ("url", "slug", "group", "name")
        extra_kwargs = {"url": {"lookup_field": "slug"}}
