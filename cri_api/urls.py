from django.urls import include, path

from rest_framework_nested import routers

from . import views
from . import nested


router = routers.DefaultRouter()
router.register("campus", views.CampusViewSet)
router.register("groups", views.CRIGroupViewSet)
router.register("users", views.CRIUserViewSet)
router.register("photos", views.UserPhotoViewSet)
router.register("photo_kind", views.UserPhotoKindViewSet)
router.register("usercreationreq", views.UserCreationRequestViewSet)

groups_router = nested.NestedDefaultRouter(router, "groups")
groups_router.register(
    "aliases", views.GroupMailAliasViewSet, basename="crigroup-aliases"
)

users_router = nested.NestedDefaultRouter(router, "users")
users_router.register("sshkeys", views.SSHPublicKeyViewSet, basename="criuser-sshkeys")
users_router.register("aliases", views.UserMailAliasViewSet, basename="criuser-aliases")
users_router.register(
    "memberships", views.UserMembershipViewSet, basename="criuser-memberships"
)
users_router.register("photos", views.UserPhotoViewSet, basename="criuser-photos")


urlpatterns = [
    path(
        "groups/<slug:slug>/members/",
        views.CRIGroupMembersView.as_view(),
        name="crigroup-members",
    ),
    path(
        "groups/<slug:slug>/history/",
        views.CRIGroupHistoryView.as_view(),
        name="crigroup-history",
    ),
    path("cards/", views.CRICardsView.as_view(), name="cricards-create"),
    path(
        "memberships/", views.MembershipCreateView.as_view(), name="membership-create"
    ),
    path("", include(router.urls)),
    path("", include(groups_router.urls)),
    path("", include(users_router.urls)),
]
