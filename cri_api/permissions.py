from django.contrib.auth import get_user_model

from rest_framework.permissions import (
    BasePermission,
    DjangoModelPermissionsOrAnonReadOnly,
)


class SwaggerPermissionOverrideMixin:
    swagger_permission_classes = []

    def get_permissions(self):
        if getattr(self, "swagger_fake_view", False):
            return [permission() for permission in self.swagger_permission_classes]
        return super().get_permissions()


class CanIssueCards(BasePermission):
    def has_permission(self, request, view=None):
        return request.user.has_perm("cri_cards.issue_card")


class CanViewRelatedAccounts(BasePermission):
    def has_permission(self, request, view=None):
        return request.user.has_perm("view_criuser_legal_identity")


class CanViewLegalIdentity(BasePermission):
    def has_permission(self, request, view=None):
        return request.user.has_perm("view_criuser_legal_identity")


class CanViewPhone(BasePermission):
    def has_permission(self, request, view=None):
        return request.user.has_perm("view_criuser_phone")


class CanViewBirthdate(BasePermission):
    def has_permission(self, request, view=None):
        return request.user.has_perm("view_criuser_birthdate")


class DjangoSelfObjectOrAnonReadOnly(DjangoModelPermissionsOrAnonReadOnly):
    user_attr = "user"
    lookup_field = "username"
    lookup_url_kwarg = "login"

    def _get_user(self, request):
        lookup_value = request.parser_context["kwargs"].get(self.lookup_url_kwarg)
        if lookup_value is None:
            return None
        try:
            return get_user_model().objects.get(**{self.lookup_field: lookup_value})
        except get_user_model().DoesNotExist:
            return None

    def has_permission(self, request, view=None):
        if request.user.is_authenticated:
            if request.method == "POST":
                user = self._get_user(request)
                if request.user != user:
                    return super().has_permission(request, view)
            return True
        return super().has_permission(request, view)

    def has_object_permission(self, request, view, obj):
        if getattr(obj, self.user_attr) == request.user:
            return True
        return super().has_permission(request, view)
