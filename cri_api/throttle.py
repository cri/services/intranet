from rest_framework import throttling


class UserRateThrottle(throttling.SimpleRateThrottle):
    """
    Imported from Django Rest Framework, rest_framework/throttling.py

    Same as DRF's UserRateThrottle class but ignores super users.
    """

    scope = "user"

    def get_cache_key(self, request, view):
        ident = None

        if request.user:
            if request.user.is_superuser:
                return None

            # Check if user has permission to bypass throttling
            if request.user.has_perm("cri_models.can_bypass_throttle"):
                return None

            if request.user.is_authenticated:
                ident = request.user.pk

        if not ident:
            ident = self.get_ident(request)

        return self.cache_format % {"scope": self.scope, "ident": ident}
