from django.db.models import Q
from django.utils import timezone
from django.core import exceptions

from rest_framework import serializers
from django_filters import rest_framework as filters
from cri_models.fields import SSHPubKey


class NumberInFilter(filters.BaseInFilter, filters.NumberFilter):
    pass


class CharInFilter(filters.BaseInFilter, filters.CharFilter):
    pass


class CRIUserFilter(filters.FilterSet):
    logins = CharInFilter(label="Logins", field_name="username")

    uids = NumberInFilter(label="UIDs", field_name="uid")

    emails = CharInFilter(
        label="Emails",
        field_name="email",
        method="emails_filtering",
    )

    first_names = CharInFilter(label="First names", field_name="first_name")

    last_names = CharInFilter(label="Last names", field_name="last_name")

    graduation_years = NumberInFilter(
        label="Graduation years",
        field_name="memberships__graduation_year",
        method="group_filtering",
    )

    groups = CharInFilter(
        label="Groups",
        field_name="memberships__group__slug",
        method="group_filtering",
    )

    ssh_key = filters.CharFilter(
        label="SSH Public Key",
        field_name="sshpublickey__key",
        method="sshkey_filtering",
    )

    @staticmethod
    def emails_filtering(queryset, name, value):
        return queryset.filter(
            Q(**{f"{name}__in": value}) | Q(mail_aliases__email__in=value)
        )

    @staticmethod
    def group_filtering(queryset, name, value):
        return queryset.filter(
            Q(memberships__end_at__gte=timezone.now())
            | Q(memberships__end_at__isnull=True),
            Q(memberships__begin_at__lte=timezone.now())
            | Q(memberships__begin_at__isnull=True),
            **{f"{name}__in": value},
        )

    @staticmethod
    def sshkey_filtering(queryset, name, value):
        try:
            key = SSHPubKey.clean(value, disallow_options=True)
            return queryset.filter(
                **{f"{name}__startswith": key},
            )
        except exceptions.ValidationError as e:
            raise serializers.ValidationError(e.messages, e.code)
