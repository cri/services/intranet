# CRI Intranet

## Developper setup

### Initial setup

You will need `docker` and `docker-compose` installed on your computer.

> :warning: Before running, you have to adjust the S3 endpoint environment
variable in the Docker Compose file. It has to be accessible from both inside
and outside the container. The IP of your computer should work. Make sure your
firewall allows incoming traffic on the S3 dev container port (8020 by default).
This is because the django-storages library does not generate signed S3 URLs
when the custom_storage setting is specified.

```sh
npm install
# keep the following open in a shell
npx webpack --mode=development --watch
```

```sh
$ poetry install
$ cd docker/
$ ./gen_secrets.sh
$ docker-compose up --build
```

Everytime you change something in the asset folder, you have to call ./manage.py
collectstatic in the intranet_dev container.

### Regular usage

```sh
$ cd docker/
$ DOCKER_BUILDKIT=1 COMPOSE_DOCKER_CLI_BUILD=1 docker-compose up --build
```

### Formatting

We use `python/black` as a code formatter. It can be enabled through
git pre-commit with the following command:

```sh
$ poetry run pre-commit install
```

### Linting

We also use `pylint` to check the coding style. It is configured with
`gitlab-ci` and produce warnings in gitlab. To check if the project
matches `pylint`'s recommendations you can use the following command:

```sh
$ poetry run prospector --profile base
```

### Services

#### Adminer
    URL: http://localhost:8010/
    System: 'PostgreSQL'
    User: 'intranet_dev'
    Password in: docker/secrets/postgres-passwd
    Database: 'intranet_dev'

#### Website
    URL: http://localhost:8000/
    Username: admin
    Password in: docker/secrets/kerberos-admin

### SPNEGO
If you want to test that SPNEGO is working properly on your machine you need
to get a ticket for a user that have a kerberos principal, for instance `admin`
has by default a relation to the principal `admin/admin`.

Then you can launch your favorite browser, configure it for SPNEGO support and
specify the location of the keytab used by SPNEGO.

#### Getting a ticket from kerberos

To get a kerberos ticket from admin/admin type the following command.
```sh
$ KRB5_CONFIG=docker/config/krb5.conf kinit admin/admin < docker/secrets/kerberos-admin
```

In general type the following command.
```sh
$ KRB5_CONFIG=docker/config/krb5.conf kinit MY_KERBEROS_USER
```

#### For chromium
Launch chromium with the following command.
```sh
$ KRB5_CONFIG=docker/config/krb5.conf KRB5_KTNAME="docker/secrets/spnego.keytab" chromium --auth-server-whitelist="localhost"
```

### For firefox
Launch firefox with the following command.
```sh
$ KRB5_CONFIG=docker/config/krb5.conf KRB5_KTNAME="docker/secrets/spnego.keytab" firefox
```

Go to `about:config` then change `network.negotiate-auth.trusted-uris` to
`localhost`.
