.. intranet documentation master file, created by
   sphinx-quickstart on Sun Jun  9 19:40:36 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Intranet's documentation
========================

.. toctree::
   :caption: Usage
   :maxdepth: 2

   usage/quickstart

.. toctree::
   :caption: Developer documentation
   :maxdepth: 2

   recipes
   oidc
   Code reference <apidoc/modules>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
