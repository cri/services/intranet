#!/bin/sh

set -e

getsecret() {
	FILE_PATH="$(eval "echo -n \"\${${1}_FILE}\"")"
	if [ ! -z "$FILE_PATH" ]; then
		cat "$FILE_PATH"
	else
		eval "echo -n \"\${${1}}\""
	fi
}

configure_s3_bucket() {
	export AWS_ACCESS_KEY_ID="$(getsecret S3_ACCESS_KEY)"
	export AWS_SECRET_ACCESS_KEY="$(getsecret S3_SECRET_KEY)"
        echo "Creating bucket ${S3_BUCKET}"
	aws --endpoint-url="${S3_ENDPOINT}" s3api create-bucket \
		--bucket="${S3_BUCKET}" --acl "private" || true
        echo "Adding CORS to bucket ${S3_BUCKET}"
	aws --endpoint-url="${S3_ENDPOINT}" s3api put-bucket-cors \
		--bucket="${S3_BUCKET}" \
		--cors-configuration="file://${CORS_CONFIG_PATH:-/app/cors.json}" || true
        echo "Adding policy to bucket ${S3_BUCKET}"
	aws --endpoint-url="${S3_ENDPOINT}" s3api put-bucket-policy \
		--bucket="${S3_BUCKET}" \
		--policy="$(sed "s/BUCKET/${S3_BUCKET}/" \
                        "${BUCKET_POLICY_PATH:-/app/bucket-policy.json}")" || true
        echo "Creating bucket ${S3_PHOTOS_BUCKET}"
	aws --endpoint-url="${S3_ENDPOINT}" s3api create-bucket \
		--bucket="${S3_PHOTOS_BUCKET}" --acl "private" || true
        echo "Adding CORS to bucket ${S3_PHOTOS_BUCKET}"
	aws --endpoint-url="${S3_ENDPOINT}" s3api put-bucket-cors \
		--bucket="${S3_PHOTOS_BUCKET}" \
		--cors-configuration="file://${CORS_CONFIG_PATH:-/app/cors.json}" || true
	unset AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY
}

init() {
	if [ -z "$SKIP_S3_CONFIGURATION" ]; then
		configure_s3_bucket
	fi

	./manage.py collectstatic --noinput
	./manage.py migrate
	./manage.py importldapschema

	if [ -n "$DEV" ] && [ "$#" -eq 0 ]; then
		./manage.py loaddata base cri_models epita || true
	fi

	./manage.py createadminuser

	if [ -n "$DEV" ] && [ "$#" -eq 0 ]; then
		./manage.py importkerberosprincipals || true
		./manage.py sync || true
	fi
}

mkdir -p $HOME/.ipython/profile_default/
cp docker/config/ipython_config.py $HOME/.ipython/profile_default/

if [ -n "$DEV" ] && [ -n "${MEILI_MASTER_KEY}" ]; then
	meili_keys="$(curl \
	  -s -X GET "${MEILI_SERVER_URI}/keys" \
	  -H "Authorization: Bearer ${MEILI_MASTER_KEY}")"
	MEILI_PUBLIC_API_KEY="$(echo "${meili_keys}" | jq -r '.results[] | select(.name == "Default Search API Key") | .key')"
	MEILI_ADMIN_API_KEY="$(echo "${meili_keys}" | jq -r '.results[] | select(.name == "Default Admin API Key") | .key')"
        echo ${MEILI_PUBLIC_API_KEY} > /tmp/MEILI_PUBLIC_API_KEY
        echo ${MEILI_ADMIN_API_KEY} > /tmp/MEILI_ADMIN_API_KEY
	export MEILI_PUBLIC_API_KEY MEILI_ADMIN_API_KEY
fi

if [ -n "$INIT_JOB" ] || ([ -n "$DEV" ] && [ "$#" -eq 0 ]); then
	init
fi

if [ -n "$CHECK_DEPLOY" ] || ([ -z "$DEV" ] && [ "$#" -eq 0 ]); then
	./manage.py check --deploy
fi

if [ "$#" -eq 0 ]; then
	if [ -n "$DEV" ]; then
		set -- ./manage.py runserver 0.0.0.0:8000
	else
		set -- gunicorn cri_intranet.wsgi -b 0.0.0.0:8000 --workers "${GUNICORN_WORKERS:-1}"
	fi
fi

exec "$@"
