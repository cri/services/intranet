from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple

from . import ldap


class LDAPGroupForm(forms.ModelForm):
    members = forms.TypedMultipleChoiceField(
        choices=lambda: ldap.LDAPUser.objects.values_list("dn", "login"),
        widget=FilteredSelectMultiple("members", is_stacked=False),
        required=False,
    )

    owners = forms.TypedMultipleChoiceField(
        choices=lambda: ldap.LDAPUser.objects.values_list("dn", "login"),
        widget=FilteredSelectMultiple("owners", is_stacked=False),
        required=False,
    )

    class Meta:
        model = ldap.LDAPGroup
        fields = "__all__"
