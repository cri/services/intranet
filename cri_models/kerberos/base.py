from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.db.models.options import Options
from django.conf import settings

import kadmin

from . import fields


class KerberosModelBase(type):
    def __new__(cls, name, bases, dct):
        newcls = super().__new__(cls, name, bases, dct)
        meta = Options(dct.get("Meta", None))
        meta.contribute_to_class(newcls, "_meta")
        for attname, field in dct.items():
            if not isinstance(field, fields.KerberosField):
                continue
            field.name = attname
            field.model = newcls
            meta.add_field(field)
            db_column = getattr(field, "db_column", attname) or attname

            def getter(field, db_column):
                def func(self):
                    # pylint: disable=protected-access
                    return newcls._get_field_value(self, field.to_python(db_column))

                return func

            prop = property(getter(field, db_column))
            if getattr(field, "editable", True):

                def setter(field, db_column):
                    def func(self, value):
                        # pylint: disable=protected-access
                        return newcls._set_field_value(
                            self, db_column, field.to_kerberos(value)
                        )

                    return func

                prop = prop.setter(setter(field, db_column))
            setattr(newcls, attname, prop)
        return newcls


class KerberosContext:
    _kadm_context = []

    class NoKADMContext(RuntimeError):
        pass

    @classmethod
    def get_context(cls, realm=None):
        if not cls._kadm_context:
            raise cls.NoKADMContext
        kadmctx = cls._kadm_context[-1]
        if realm is None:
            realm = settings.KERBEROS_DEFAULT_REALM
        if realm not in kadmctx:
            kadmctx[realm] = kadmin.init_with_keytab(
                settings.KERBEROS_REALMS[realm]["ADMIN_PRINCIPAL"],
                settings.KERBEROS_ADMIN_KEYTAB,
                {},
                realm,
            )
        return kadmctx[realm]

    def __init__(self, reuse_context=False):
        self.reuse_context = reuse_context

    def __enter__(self):
        if not self.reuse_context or not self._kadm_context:
            self.reuse_context = False
            self._kadm_context.append({})
        return self

    def __exit__(self, *_args, **_kwargs):
        if not self.reuse_context:
            self._kadm_context.pop()


class KerberosModel(metaclass=KerberosModelBase):
    class DoesNotExist(ObjectDoesNotExist):
        pass

    class MultipleObjectsReturned(MultipleObjectsReturned):
        pass

    def __init__(self, _pk=None, realm=None, **kwargs):
        self.pk = _pk
        self._instance = None
        self._data = {}
        field_names = [f.name for f in self._meta.get_fields()]
        if not _pk:
            for k, v in kwargs.items():
                if k in field_names:
                    setattr(self, k, v)
        if realm is None:
            realm = settings.KERBEROS_DEFAULT_REALM
        self.realm = realm

    def __repr__(self):
        return f"<{self.__class__.__name__}: {self}>"

    def __str__(self):
        return f"{self.__class__.__name__} object ({self.pk})"

    def refresh_from_db(self):
        raise NotImplementedError

    def save(self, force_insert=False, force_update=False, update_fields=None):
        raise NotImplementedError

    def delete(self):
        raise NotImplementedError

    def _get_field_value(self, fieldname):
        return self._data.get(fieldname, None)

    def _set_field_value(self, fieldname, value):
        self._data[fieldname] = value

    def _is_field_modified(self, fieldname):
        return fieldname in self._data

    @classmethod
    def _get_kadm(cls, realm=None):
        return KerberosContext.get_context(realm)

    @classmethod
    def all(cls, realm=None):
        raise NotImplementedError

    @classmethod
    def get(cls, pk, realm=None):
        objects = [o for o in cls.all(realm) if o.pk == pk]
        if not objects:
            raise cls.DoesNotExists
        if len(objects) > 1:
            raise cls.MultipleObjectReturned
        return objects[0]
