from .modelengine import (
    RemoteModelSyncMixin,
    SyncRemoteModelEngine,
    SyncedModelAdapter,
)


class LDAPRemoteModelSyncMixin(RemoteModelSyncMixin):
    def get_remote_obj(self, remote_obj_id):
        return self.remote_model.objects.get(dn=remote_obj_id)


class SyncRemoteLDAPModelEngine(LDAPRemoteModelSyncMixin, SyncRemoteModelEngine):
    pass


class SyncedLDAPModelAdapter(SyncedModelAdapter):
    sync_engine_class = SyncRemoteLDAPModelEngine

    @classmethod
    def get_sync_objects_map(cls, local_objects, remote_objects):
        obj_map = {}
        remote_objects_id = set(remote_obj.dn for remote_obj in remote_objects)
        for local_obj in local_objects:
            mapping = local_obj.get_sync_remote_obj_id_multi(remote_objects)
            for local_obj_id, remote_obj_id in mapping:
                if None in (local_obj_id, remote_obj_id):
                    continue
                if remote_obj_id in remote_objects_id:
                    obj_map[local_obj_id] = remote_obj_id
        return obj_map
