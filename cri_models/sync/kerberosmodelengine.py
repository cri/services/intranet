from .. import kerberos
from .modelengine import (
    RemoteModelSyncMixin,
    SyncRemoteModelEngine,
    SyncedModelAdapter,
)


class KerberosModelSyncMixin(RemoteModelSyncMixin):
    def get_remote_obj(self, remote_obj_id):
        return self.remote_model.get(pk=remote_obj_id)

    def delete_remote_obj(self, remote_obj_id, *, pretend=False):
        remote_obj = self.get_remote_obj(remote_obj_id)
        if not pretend:
            remote_obj.delete()


class KerberosPrincipalSyncMixin(KerberosModelSyncMixin):
    def delete_remote_obj(self, remote_obj_id, *, pretend=False):
        if "/" in remote_obj_id and "/test" not in remote_obj_id:
            raise NotImplementedError(
                "Kerberos sync deletion is disabled for principals with an instance"
            )
        super().delete_remote_obj(remote_obj_id, pretend=pretend)


class SyncKerberosPrincipalEngine(KerberosPrincipalSyncMixin, SyncRemoteModelEngine):
    pass


class SyncedKerberosPrincipalAdapter(SyncedModelAdapter):
    sync_engine_class = SyncKerberosPrincipalEngine
    sync_remote_model = kerberos.Principal

    @classmethod
    def _partial_sync(cls, local_objects, *, pretend=False):
        with cls.get_sync_engine() as sync_engine:
            remote_objects = []
            for obj in local_objects:
                try:
                    remote_objects.append(
                        sync_engine.get_remote_obj(obj.get_sync_remote_obj_id(None))
                    )
                except cls.sync_remote_model.DoesNotExist:
                    continue
            obj_map = cls.get_sync_objects_map(local_objects, remote_objects)
            remote_data = dict(map(cls.get_sync_remote_data, remote_objects))
            local_data = dict(map(cls.get_sync_local_data, local_objects))
            remote_data = {
                remote_obj_id: remote_data
                for remote_obj_id, remote_data in remote_data.items()
                if remote_obj_id in obj_map.values()
            }
            return sync_engine.sync(local_data, remote_data, obj_map, pretend=pretend)

    @classmethod
    def _sync(cls, local_objects, *, pretend=False, partial=False):
        with kerberos.KerberosContext(reuse_context=True):
            # We use a faster method of retrieving a small batch of remote
            # objects when performing partial synchronization.
            if partial and len(local_objects) < 10:
                return cls._partial_sync(local_objects, pretend=pretend)
            return super()._sync(local_objects, pretend=pretend, partial=partial)

    @classmethod
    def get_sync_remote_queryset(cls):
        return list(cls.get_sync_remote_model().all())
