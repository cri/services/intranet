class SyncEngine:
    """Base class for synchronization engines"""

    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs

    def __enter__(self):
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        pass

    def add_local_obj(self, local_obj_id, local_obj_data, *, pretend=False):
        raise NotImplementedError

    def bulk_add_local_objs(self, local_objs, *, pretend=False):
        added = {}
        failed = {}
        for local_obj_id, local_obj_data in local_objs.items():
            try:
                obj = self.add_local_obj(local_obj_id, local_obj_data, pretend=pretend)
                added[local_obj_id] = obj
            except Exception as e:
                failed[local_obj_id] = e
        return added, failed

    def add_remote_obj(self, remote_obj_id, remote_obj_data, *, pretend=False):
        raise NotImplementedError

    def bulk_add_remote_objs(self, remote_objs, *, pretend=False):
        added = {}
        failed = {}
        for remote_obj_id, remote_obj_data in remote_objs.items():
            try:
                obj = self.add_remote_obj(
                    remote_obj_id, remote_obj_data, pretend=pretend
                )
                added[remote_obj_id] = obj
            except Exception as e:
                failed[remote_obj_id] = e
        return added, failed

    def get_obj_diff(
        self, local_obj_id, local_obj_data, remote_obj_id, remote_obj_data
    ):  # pylint: disable=unused-argument
        """Compare two existing objects and determines which fields need
        to be updated accordingly.

        :return: a mapping of field names to an ``(old_value,
                 new_value)`` tuple."""
        keys = set(local_obj_data.keys()) & set(remote_obj_data.keys())
        return {
            k: (remote_obj_data[k], local_obj_data[k])
            for k in keys
            if local_obj_data[k] != remote_obj_data[k]
        }

    def update_obj(
        self,
        local_obj_id,
        local_obj_data,
        remote_obj_id,
        remote_obj_data,
        *,
        pretend=False,
    ):
        raise NotImplementedError

    def bulk_update_objs(self, objs, *, pretend=False):
        updated = {}
        failed = {}
        for local_obj, remote_obj in objs:
            try:
                changes = self.update_obj(*local_obj, *remote_obj, pretend=pretend)
                updated[local_obj[0]] = changes
            except Exception as e:
                failed[local_obj[0]] = e
        return updated, failed

    def delete_local_obj(self, local_obj_id, *, pretend=False):
        raise NotImplementedError

    def bulk_delete_local_objs(self, local_objs, *, pretend=False):
        deleted = set()
        failed = {}
        for local_obj_id in local_objs.keys():
            try:
                self.delete_local_obj(local_obj_id, pretend=pretend)
                deleted.add(local_obj_id)
            except Exception as e:
                failed[local_obj_id] = e
        return deleted, failed

    def delete_remote_obj(self, remote_obj_id, *, pretend=False):
        raise NotImplementedError

    def bulk_delete_remote_objs(self, remote_objs, *, pretend=False):
        deleted = set()
        failed = {}
        for remote_obj_id in remote_objs.keys():
            try:
                self.delete_remote_obj(remote_obj_id, pretend=pretend)
                deleted.add(remote_obj_id)
            except Exception as e:
                failed[remote_obj_id] = e
        return deleted, failed

    def sync(self, local_objects, remote_objects, obj_map, *, pretend=False):
        raise NotImplementedError


class UnidirectionalSyncEngine(SyncEngine):
    """This synchronization engine always brings the remote objects to
    the state of the local objects."""

    def sync(self, local_objects, remote_objects, obj_map, *, pretend=False):
        """Synchronize objects.

        The synchronization is done according to a three steps process:
          1. We remove the remote objects who are not related to a local
             objects.
          2. We create the missing remote objects for which a
             corresponding local objects exists.
          3. We update the remote objects for which a corresponding
             local objects exists.

        :param local_objects: a mapping between local objects ID and
                              data
        :param remote_objects: a mapping between remote objects ID
                               and data
        :param obj_map: a mapping between existing local objects IDs and
                        existing remote objects IDs.
        :param pretend: if True no action is effectively performed.

        :return: a tuple containing added, updated, deleted objects and
                 failed operations
        """
        obj_map = {
            local_obj_id: remote_obj_id
            for local_obj_id, remote_obj_id in obj_map.items()
            if local_obj_id in local_objects
            if remote_obj_id in remote_objects
        }

        to_add = {
            obj_id: obj
            for obj_id, obj in local_objects.items()
            if obj_id not in obj_map.keys()
        }

        to_delete = {
            remote_obj_id: remote_obj
            for remote_obj_id, remote_obj in remote_objects.items()
            if remote_obj_id not in obj_map.values()
        }

        to_update = [
            (
                (local_obj_id, local_objects[local_obj_id]),
                (remote_obj_id, remote_objects[remote_obj_id]),
            )
            for local_obj_id, remote_obj_id in obj_map.items()
        ]

        deleted = self.bulk_delete_remote_objs(to_delete, pretend=pretend)
        updated = self.bulk_update_objs(to_update, pretend=pretend)
        added = self.bulk_add_remote_objs(to_add, pretend=pretend)

        return added, updated, deleted
