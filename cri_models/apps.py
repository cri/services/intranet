from django.apps import AppConfig


class CRIModelsConfig(AppConfig):
    name = "cri_models"
    verbose_name = "CRI models"

    def ready(self):
        from django.db.models.signals import post_save, pre_delete
        from . import meili

        def add_to_index(sender, instance, **kwargs):
            instance.add_to_index()

        def remove_from_index(sender, instance, **kwargs):
            instance.remove_from_index()

        for model in meili.MeilisearchIndexedMixin.__subclasses__():
            model.setup_index()
            post_save.connect(add_to_index, sender=model)
            pre_delete.connect(remove_from_index, sender=model)

        return super().ready()
