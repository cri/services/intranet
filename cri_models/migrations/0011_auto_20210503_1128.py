# Generated by Django 3.1.7 on 2021-05-03 09:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("cri_models", "0010_auto_20210313_1311"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="criusernote",
            options={
                "get_latest_by": "created_at",
                "ordering": ("-created_at",),
                "verbose_name": "CRI User note",
            },
        ),
    ]
