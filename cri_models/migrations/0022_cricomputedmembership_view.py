import cri_models.fields
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("cri_models", "0021_auto_20230208_1659"),
    ]

    operations = [
        migrations.DeleteModel(
            name="CRICurrentComputedMembership",
        ),
        migrations.DeleteModel(
            name="CRIComputedMembership",
        ),
        migrations.RemoveField(
            model_name="crigroup",
            name="computed_children",
        ),
        migrations.CreateModel(
            name="CRIGroupComputedChildren",
            fields=[
                ("id", models.CharField(primary_key=True, serialize=False)),
            ],
            options={
                "db_table": "cri_models_crigroup_computed_children",
                "managed": False,
            },
        ),
        migrations.CreateModel(
            name="CRIComputedMembership",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "graduation_year",
                    cri_models.fields.GraduationYearField(
                        blank=True,
                        null=True,
                        validators=[django.core.validators.MinValueValidator(1989)],
                    ),
                ),
                ("begin_at", models.DateField(blank=True, null=True)),
                ("end_at", models.DateField(blank=True, null=True)),
            ],
            options={
                "verbose_name": "CRI Computed Membership",
                "db_table": "cri_models_cricomputedmembership",
                "managed": False,
            },
        ),
        migrations.CreateModel(
            name="CRICurrentComputedMembership",
            fields=[
                (
                    "graduation_year",
                    cri_models.fields.GraduationYearField(
                        blank=True,
                        null=True,
                        validators=[django.core.validators.MinValueValidator(1989)],
                    ),
                ),
                ("begin_at", models.DateField(blank=True, null=True)),
                ("end_at", models.DateField(blank=True, null=True)),
                ("id", models.CharField(primary_key=True, serialize=False)),
            ],
            options={
                "verbose_name": "CRI Current Computed Memberships",
                "verbose_name_plural": "CRI Current Computed Memberships",
                "db_table": "cri_models_cricurrentcomputedmembership",
                "managed": False,
            },
        ),
        migrations.RunSQL(
            """
            CREATE MATERIALIZED VIEW cri_models_crigroup_computed_children AS
                SELECT
                    CONCAT(p.from_crigroup_id, '.', p.to_crigroup_id) AS id,
                    p.*
                FROM (
                    WITH RECURSIVE parents(computed_parent, children, leaf) AS (
                        SELECT
                            from_crigroup_id,
                            to_crigroup_id,
                            to_crigroup_id
                        FROM
                            cri_models_crigroup_children
                        UNION
                        SELECT
                            r.from_crigroup_id,
                            r.to_crigroup_id,
                            p.leaf
                        FROM
                            cri_models_crigroup_children r
                            INNER JOIN parents p ON r.to_crigroup_id = p.computed_parent
                    )
                    SELECT DISTINCT
                        parents.computed_parent from_crigroup_id,
                        parents.leaf to_crigroup_id
                    FROM
                        parents
                ) p;
        """,
            "DROP MATERIALIZED VIEW cri_models_crigroup_computed_children;",
        ),
        migrations.AddField(
            model_name="crigroup",
            name="computed_children",
            field=models.ManyToManyField(
                blank=True,
                editable=False,
                related_name="computed_parents",
                through="cri_models.CRIGroupComputedChildren",
                to="cri_models.crigroup",
            ),
        ),
        migrations.RunSQL(
            """
            CREATE MATERIALIZED VIEW cri_models_cricomputedmembership AS
                SELECT
                    CONCAT(m.user_id, '/', computed_children.from_crigroup_id, '/', m.graduation_year) id,
                    m.graduation_year,
                    NULLIF(MIN(COALESCE(m.begin_at, '-infinity'::date)), '-infinity'::date) begin_at,
                    NULLIF(MAX(COALESCE(m.end_at, 'infinity'::date)), 'infinity'::date) end_at,
                    computed_children.from_crigroup_id group_id,
                    m.user_id
                FROM
                    (
                        SELECT
                            m.graduation_year,
                            m.begin_at,
                            m.end_at,
                            m.group_id,
                            m.user_id
                        FROM
                            cri_models_crimembership m
                        UNION
                        SELECT
                            NULL,
                            NULL,
                            NULL,
                            u.primary_group_id,
                            u.username
                        FROM
                            cri_models_criuser u
                    ) m
                    LEFT JOIN (
                        SELECT
                            computed_children.from_crigroup_id,
                            computed_children.to_crigroup_id
                        FROM
                            cri_models_crigroup_computed_children computed_children
                        UNION
                        SELECT
                            g.id,
                            g.id
                        FROM
                            cri_models_crigroup g
                    ) computed_children ON computed_children.to_crigroup_id = m.group_id
                GROUP BY
                    computed_children.from_crigroup_id,
                    m.graduation_year,
                    m.user_id;
        """,
            "DROP MATERIALIZED VIEW cri_models_cricomputedmembership;",
        ),
        migrations.RunSQL(
            """
            CREATE VIEW cri_models_cricurrentcomputedmembership AS
                SELECT
                    *
                FROM
                    cri_models_cricomputedmembership
                WHERE
                    (begin_at IS NULL OR begin_at <= NOW())
                    AND
                    (end_at IS NULL OR end_at >= NOW());
        """,
            "DROP VIEW cri_models_cricurrentcomputedmembership;",
        ),
    ]
