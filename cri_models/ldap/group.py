from ldapdb.models import fields

import ldap
import logging

from .models import LDAPModel
from .user import LDAPUser


_logger = logging.getLogger(__name__)


class LDAPGroup(LDAPModel):

    # LDAP metadata
    relative_base_dn = "ou=groups"

    object_classes = ("groupOfMembers",)

    classes = fields.ListField(
        db_column="objectClass",
        editable=False,
        verbose_name="Object class list",
        default=object_classes,
    )

    # groupOfMembers attributes
    name = fields.CharField(db_column="cn", max_length=200, primary_key=True)

    members = fields.ListField(db_column="member", blank=True)

    managers = fields.ListField(db_column="owner", blank=True)

    description = fields.CharField(db_column="description", max_length=200, blank=True)

    # posixGroup attributes
    gid = fields.IntegerField(
        db_column="gidNumber", unique=True, verbose_name="GID", blank=True, null=True
    )

    # mailAccount attributes (we are using extensibleObject to avoid the
    # requirement to set a `mail` attribute).
    mail_aliases = fields.ListField(db_column="mailalias", blank=True)

    class Meta:
        verbose_name = "LDAP group"

    class LDAPMeta:
        object_classes_map = {
            "posixGroup": (("gid",), ()),
            "extensibleObject": ((), ("mail_aliases",)),
        }

    def get_members(self):
        for dn in self.members:  # pylint: disable=not-an-iterable
            try:
                yield LDAPUser.objects.get(dn=dn)
            except (LDAPUser.DoesNotExist, ldap.NO_SUCH_OBJECT):
                _logger.error(
                    "LDAP group '%s' contains an invalid DN: '%s'", self.dn, dn
                )

    def get_managers(self):
        for dn in self.managers:  # pylint: disable=not-an-iterable
            try:
                yield LDAPUser.objects.get(dn=dn)
            except (LDAPUser.DoesNotExist, ldap.NO_SUCH_OBJECT):
                _logger.error(
                    "LDAP group '%s' contains an invalid DN: '%s'", self.dn, dn
                )

    def __str__(self):
        return self.name
