from .criuser import (
    CRIUser,
    SSHPublicKey,
    KerberosPrincipal,
    CRIPhotoKind,
    CRIPhoto,
    CRIUserPreference,
)
from .crigroup import (
    CRIGroup,
    CRIMembership,
    CRIComputedMembership,
    CRICurrentComputedMembership,
    Campus,
)
from .criserviceaccount import CRIServiceAccount
from .criusercreationrequest import CRIUserCreationRequestScope, CRIUserCreationRequest
from .criusernote import CRIUserNoteScope, CRIUserNoteLabel, CRIUserNote
from .mail import MailAlias

__all__ = (
    "CRIComputedMembership",
    "CRICurrentComputedMembership",
    "CRIGroup",
    "CRIMembership",
    "CRIServiceAccount",
    "CRIUser",
    "CRIUserCreationRequestScope",
    "CRIUserCreationRequest",
    "CRIUserNote",
    "CRIUserNoteLabel",
    "CRIUserNoteScope",
    "Campus",
    "KerberosPrincipal",
    "MailAlias",
    "SSHPublicKey",
    "CRIPhotoKind",
    "CRIPhoto",
    "CRIUserPreference",
)
