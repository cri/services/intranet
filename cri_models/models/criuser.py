from django.apps import apps
from django.db import models, transaction
from django.core.exceptions import ValidationError
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.utils import timezone
from django.urls import reverse
from django.templatetags.static import static
import uuid
from PIL import Image, ImageOps
from tempfile import NamedTemporaryFile
from enum import Enum
import face

from phonenumber_field.modelfields import PhoneNumberField

from .crigroup import CRIGroup, CRIComputedMembership, CRICurrentComputedMembership
from .. import ldap, kerberos, fields, sync, meili

from cri_intranet.custom_storage import PhotoStorage

from datetime import datetime
from unidecode import unidecode
import logging

_logger = logging.getLogger(__name__)


class CRIUserLDAPSyncAdapter(sync.SyncedLDAPModelAdapter):
    sync_remote_model = ldap.LDAPUser

    def get_sync_remote_obj_id(self, _remote_objects):
        return self.obj.ldap_dn

    def get_sync_local_data(self):
        SYNC_FIELDS = ("email", "first_name", "ldap_dn", "uid")
        data = {
            "full_name": self.obj.get_full_name(),
            "home": f"/home/{self.obj.username}",
            "last_name": self.obj.last_name or self.obj.username,
            "login": self.obj.username,
            "passwords": set(),
            **{f: getattr(self.obj, f) for f in SYNC_FIELDS},
        }
        if self.obj.primary_principal:
            data["passwords"].add(f"{{SASL}}{self.obj.primary_principal}")
        if self.obj.is_auth_allowed() and self.obj.password.startswith("argon2$"):
            data["passwords"].add(self.obj.password.replace("argon2$", "{ARGON2}$"))
        if self.obj.primary_group.gid:
            data["gid"] = self.obj.primary_group.gid
        if self.obj.is_active:
            data["ssh_keys"] = set(
                k.key.keydata for k in self.obj.sshpublickey_set.all()
            )
        else:
            data["ssh_keys"] = set()
        data["mail_aliases"] = set(self.obj.get_mail_aliases())
        return (self.obj.pk, data)

    @staticmethod
    def get_sync_remote_data(remote_obj):
        SYNC_FIELDS = (
            "email",
            "first_name",
            "full_name",
            "gid",
            "home",
            "last_name",
            "login",
            "uid",
        )
        data = {
            "ssh_keys": set(remote_obj.ssh_keys),
            "mail_aliases": set(remote_obj.mail_aliases),
            "passwords": set(remote_obj.passwords),
            **{f: getattr(remote_obj, f) for f in SYNC_FIELDS},
        }
        return (remote_obj.dn, data)

    @classmethod
    def get_sync_local_queryset(_cls, model):
        return model.objects.filter(ldap_dn__isnull=False).exclude(ldap_dn="")


class CRIUser(sync.SyncedModelMixin, meili.MeilisearchIndexedMixin, AbstractUser):
    LDAP_AUTH_DN_FIELD = "ldap_dn"
    REQUIRED_FIELDS = ("email", "uid")
    DEFAULT_GROUP_PK = 1

    _to_sync = set()

    username = models.CharField(primary_key=True, max_length=100)

    first_name = models.CharField(
        max_length=100, blank=True, help_text="commonly used first name"
    )

    last_name = models.CharField(
        max_length=100, blank=False, help_text="commonly used last name"
    )

    legal_first_name = models.CharField(
        max_length=100, blank=True, help_text="first name to be used on legal documents"
    )

    legal_last_name = models.CharField(
        max_length=100, blank=True, help_text="last name to be used on legal documents"
    )

    uid = fields.UnixIDField(unique=True, help_text="Unix user ID", verbose_name="UID")

    primary_group = models.ForeignKey(
        "CRIGroup",
        on_delete=models.SET_DEFAULT,
        default=DEFAULT_GROUP_PK,
        related_name="primary_group_user",
        limit_choices_to={"gid__isnull": False},
    )

    birthdate = models.DateField(blank=True, null=True)

    phone = PhoneNumberField(blank=True, null=True, help_text="Mobile phone number")

    old_account = models.OneToOneField(
        "self",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="new_account",
        help_text="Past account of this user",
    )

    ldap_dn = ldap.LDAPDNField(
        ldap.LDAPUser,
        max_length=254,
        blank=True,
        null=True,
        unique=True,
        verbose_name="LDAP DN",
    )

    primary_principal = models.ForeignKey(
        "KerberosPrincipal",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        help_text="Primary kerberos principal of this user",
    )

    mail_aliases = models.ManyToManyField(
        "MailAlias", blank=True, help_text="Mail aliases that can be used by this user"
    )

    nickname = models.CharField(max_length=100, blank=True)

    callsign = models.CharField(
        max_length=16, blank=True, help_text="Amateur radio operator call sign"
    )

    class Meta(AbstractUser.Meta):
        verbose_name = "CRI User"
        verbose_name_plural = "CRI Users"
        ordering = ("username",)
        permissions = (
            ("impersonate", "Can impersonate users"),
            ("view_criuser_legal_identity", "Can view legal identies"),
            ("view_criuser_phone", "can view phone numbers"),
            ("view_criuser_birthdate", "Can view birthdates"),
            ("update_criuser_password", "Can change passwords"),
            ("export_data", "Can export user data"),
            ("view_criuser_photo", "Can view all photos"),
            ("can_bypass_throttle", "Can bypass the rate limit system"),
        )

    class MeiliMeta(meili.MeilisearchIndexedMixin.MeiliMeta):
        index_name = "users"
        primary_key = "index_key"
        indexed_fields = (
            "index_key",
            "username",
            "get_full_name",
            "first_name",
            "last_name",
            "email",
            "uid",
            "graduation_years",
            "picture_square_acl",
            "get_old_accounts",
            "has_new_account",
            "nickname",
            "callsign",
            "type",
            "obj_url",
        )
        renamed_fields = (
            ("picture_square_acl", "picture"),
            ("get_full_name", "name"),
            ("get_old_accounts", "old_accounts"),
        )
        filterable_attributes = ("has_new_account",)
        searchable_attributes = (
            "username",
            "first_name",
            "last_name",
            "email",
            "uid",
            "old_accounts",
            "nickname",
            "callsign",
        )

    class SyncMeta:
        sync_adapters = (CRIUserLDAPSyncAdapter,)

    @classmethod
    def delayed_sync(cls, user):
        cls._to_sync.add(user)

        def do_sync():
            if cls._to_sync:
                cls.partial_sync(cls._to_sync)
            cls._to_sync.clear()

        return do_sync

    @property
    def graduation_years(self):
        return list(
            CRICurrentComputedMembership.objects.filter(
                group__in=self.get_groups(), graduation_year__isnull=False
            )
            .distinct("graduation_year")
            .order_by("graduation_year")
            .values_list("graduation_year", flat=True)
        )

    @property
    def index_key(self):
        return self.username.replace(".", "_")

    @property
    def has_new_account(self):
        return bool(self.get_new_account())

    @property
    def picture_full_acl(self):
        return reverse("criphoto", kwargs={"username": self.username})

    @property
    def picture_thumb_acl(self):
        return reverse("criphoto_thumb", kwargs={"username": self.username})

    @property
    def picture_square_acl(self):
        return reverse("criphoto_square", kwargs={"username": self.username})

    @property
    def picture_full(self):
        return self.get_photo().image.url

    @property
    def picture_thumb(self):
        return self.get_photo().image_thumb.url

    @property
    def picture_square(self):
        return self.get_photo().image_square.url

    @property
    def printable_name(self):
        return (
            " ".join(filter(None, (self.last_name.upper(), self.first_name)))
            or self.username
        )

    @property
    def fs_name(self):
        return unidecode(f"{self.printable_name}_{self.uid}").replace(" ", "_")

    @property
    def last_activity(self):
        last_entry = (
            self.connectionlogentry_set.all()
            .filter(status="success")
            .order_by("-created_at")
            .first()
        )
        if last_entry:
            return last_entry.created_at

        return None

    def get_user_creation_request_scopes(self):
        return (
            apps.get_model("cri_models", "CRIUserCreationRequestScope")
            .objects.filter(groups__in=self.get_groups())
            .distinct()
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.pk is None:
            self._old_primary_group_id = None
            self._old_is_active = None
        else:
            self._old_primary_group_id = self.primary_group_id
            self._old_is_active = self.is_active

    def is_auth_allowed(self):
        if hasattr(self, "criserviceaccount"):
            return self.criserviceaccount.is_auth_allowed()
        if not self.is_active:
            _logger.info("User '%s' is disabled", self.username)
            return False
        return True

    def get_full_name(self):
        return super().get_full_name() or self.username

    def get_absolute_url(self):
        return reverse("criuser_detail", kwargs={"username": self.username})

    def get_mail_aliases(self):
        return self.mail_aliases.all().values_list("email", flat=True)

    def get_old_accounts(self):
        old_accounts = []
        current = self
        while hasattr(current, "old_account") and current.old_account:
            current = current.old_account
            if current in old_accounts:
                _logger.warning(
                    "User '%s' is part of an old_accounts loop: %s",
                    str(self),
                    ", ".join(map(str, old_accounts)),
                )
                break
            old_accounts.append(current)
        return old_accounts

    def get_new_account(self):
        new_accounts = [self]
        current = self
        while hasattr(current, "new_account") and current.new_account:
            current = current.new_account
            if current in new_accounts:
                _logger.warning(
                    "User '%s' is part of an old_accounts loop: %s",
                    str(self),
                    ", ".join(map(str, new_accounts)),
                )
                return None
            new_accounts.append(current)
        if current is not self:
            return current
        return None

    def get_groups(self):
        return CRIGroup.objects.filter(
            models.Q(memberships__end_at__gte=timezone.now())
            | models.Q(memberships__end_at__isnull=True),
            models.Q(memberships__begin_at__lte=timezone.now())
            | models.Q(memberships__begin_at__isnull=True),
            memberships__user=self,
        ).distinct()

    def get_past_groups(self):
        return (
            CRIGroup.objects.filter(
                memberships__end_at__lt=timezone.now(), memberships__user=self
            )
            .exclude(
                models.Q(memberships__begin_at__gte=timezone.now())
                | models.Q(memberships__end_at__isnull=True)
            )
            .distinct()
        )

    def get_future_groups(self):
        return CRIGroup.objects.filter(
            memberships__begin_at__gt=timezone.now(), memberships__user=self
        ).distinct()

    def get_sorted_photos(self):
        return self.criphoto_set.order_by("-kind__priority", "-priority")

    def get_photo(self):
        return self.get_sorted_photos().first()

    def _sync_kerberos_password(self, password, raise_on_primary_error=True):
        principals = set(self.kerberosprincipal_set.filter(out_of_date=True))
        for principal in principals:
            with transaction.atomic():
                try:
                    principal.set_and_save_password(password)
                except Exception as e:
                    _logger.error(
                        "a problem occured during the password synchronization "
                        "of principal '%s' for user '%s': %s",
                        principal,
                        self.username,
                        e,
                    )
                    if raise_on_primary_error and principal == self.primary_principal:
                        raise

    def check_password(self, raw_password):
        if not super().check_password(raw_password):
            return False
        self._sync_kerberos_password(raw_password, raise_on_primary_error=False)
        return True

    def clean_fields(self, exclude=None):
        super().clean_fields(exclude)
        if exclude is None or "primary_principal" not in exclude:
            primary = self.primary_principal
            if primary and primary.user is not None and primary.user != self:
                raise ValidationError(
                    "Principal '%(principal)s' is the primary principal of"
                    "'%(user1)s' but is assigned to '%(user2)s'!",
                    code="principal_owned_by_another_user",
                    params={
                        "principal": primary.principal,
                        "user1": primary.user,
                        "user2": self,
                    },
                )

    @transaction.atomic
    # pylint: disable=arguments-differ
    def save(self, *args, raise_on_primary_error=True, **kwargs):
        if not self.ldap_dn:
            ldap_dn = ldap.LDAPUser(login=self.username).build_dn()
            if type(self).objects.filter(ldap_dn=ldap_dn).exists():
                _logger.warning(
                    "User '%s' saved without ldap_dn because another user "
                    "is already assigned '%s!",
                    str(self),
                    ldap_dn,
                )
            else:
                self.ldap_dn = ldap_dn
        if self.primary_principal:
            self.primary_principal.user = self
            self.primary_principal.save()
        else:
            principals = self.kerberosprincipal_set.all()
            if len(principals) == 1:
                self.primary_principal = principals[0]
        password = self._password
        super().save(*args, **kwargs)
        if not hasattr(self, "criuserpreference"):
            user_pref = CRIUserPreference(user=self)
            user_pref.save()
        if self.primary_group_id != self._old_primary_group_id:
            CRIComputedMembership.compute_entries()
        if password:
            self.kerberosprincipal_set.update(out_of_date=True)
            self._sync_kerberos_password(
                password, raise_on_primary_error=raise_on_primary_error
            )
            self.sync()
            self._password = None
        elif self.is_active != self._old_is_active:
            self.sync()
            KerberosPrincipal.partial_sync(self.kerberosprincipal_set.all())
        self._old_primary_group_id = self.primary_group_id
        self._old_is_active = self.is_active

    @staticmethod
    def get_next_available_uid(group):
        next_group = CRIGroup.objects.filter(gid__gt=group.gid).order_by("gid").first()

        qs = CRIUser.objects.filter(uid__gt=group.gid)
        if next_group:
            qs = qs.filter(uid__lt=next_group.gid)

        previous_user = qs.order_by("-uid").first()
        uid = group.gid + 1
        if previous_user:
            uid = previous_user.uid + 1

        if next_group and uid >= next_group.gid:
            raise RuntimeError("Found uid is above possible group uids")

        return uid


class CRIUserPreference(models.Model):
    class PhotoVisibility(models.IntegerChoices):
        PRIVATE = 1
        INTERNAL = 2
        PUBLIC = 3

    user = models.OneToOneField(CRIUser, on_delete=models.CASCADE)
    photo_visibility = models.IntegerField(
        default=PhotoVisibility.INTERNAL, choices=PhotoVisibility.choices
    )

    class Meta:
        verbose_name = "CRI User Preference"


class CRIPhotoKind(models.Model):
    name = models.CharField(primary_key=True, max_length=20)
    priority = models.SmallIntegerField()

    class Meta:
        verbose_name = "CRI Photo Kind"
        verbose_name_plural = "CRI Photo Kinds"

    def __str__(self):
        return self.name


class CRIPhoto(models.Model):
    @staticmethod
    def get_photo_path(instance, _):
        return f"{instance.kind.name}/{instance.user.username}_{instance.uuid}.jpg"

    @staticmethod
    def get_photo_thumb_path(instance, _):
        return (
            f"{instance.kind.name}/{instance.user.username}_{instance.uuid}-thumb.jpg"
        )

    @staticmethod
    def get_photo_square_path(instance, _):
        return (
            f"{instance.kind.name}/{instance.user.username}_{instance.uuid}-square.jpg"
        )

    @staticmethod
    def get_blank_path():
        return static("img/blank.jpg")

    class PhotoCrop(Enum):
        FULL = 1
        THUMB = 2
        SQUARE = 3

    @classmethod
    def from_db(cls, db, field_names, values):
        instance = super().from_db(db, field_names, values)
        instance.save_image_state()
        return instance

    @staticmethod
    def save_image_to_field(image, field):
        with NamedTemporaryFile() as tmpfile:
            image = image.convert("RGB")
            image.save(tmpfile, format="JPEG", quality=70)
            field.save("noname.jpg", tmpfile, save=False)

    def save_image_state(self):
        self._old_image = self.image  # Save current image state
        self._old_image_square = self.image_square

    def save_thumb(self, img):
        """Saves the thumbnail of a CRIPhoto.

        Parameters:
        img: Opened PIL image to create the thumb from
        """
        s = settings.PHOTO_THUMB_WIDTH
        thumb = img.resize((s, s * img.height // img.width))
        CRIPhoto.save_image_to_field(thumb, self.image_thumb)

    def save_face(self, img):
        """Saves the face crop of a CRIPhoto.

        Parameters:
        img: Opened PIL image to create the crop from
        """
        try:
            s = settings.PHOTO_SQUARE_SIZE
            face_img = face.crop_image(img, 1.0, 0.5, (s, s))
            CRIPhoto.save_image_to_field(face_img, self.image_square)
        except Exception:
            # Remove old square to avoid mismatch if full image was changed
            self.image_square = None

    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(CRIUser, on_delete=models.CASCADE)
    kind = models.ForeignKey(CRIPhotoKind, on_delete=models.CASCADE)
    image = models.ImageField(storage=PhotoStorage, upload_to=get_photo_path.__func__)
    image_thumb = models.ImageField(
        storage=PhotoStorage,
        upload_to=get_photo_thumb_path.__func__,
        editable=False,
        blank=True,
    )
    image_square = models.ImageField(
        storage=PhotoStorage, upload_to=get_photo_square_path.__func__, blank=True
    )
    priority = models.SmallIntegerField(blank=True)

    _old_image = None
    _old_image_square = None

    class Meta:
        verbose_name = "CRI Photo"
        verbose_name_plural = "CRI Photos"

    def clean_fields(self, exclude=None):
        super().clean_fields(exclude)
        if exclude is None or "priority" not in exclude:
            if (
                self.priority is not None
                and CRIPhoto.objects.filter(
                    user=self.user, kind=self.kind, priority=self.priority
                )
                .exclude(pk=self.pk)
                .exists()
            ):
                raise ValidationError(
                    f"User {self.user.username} already has a photo with kind "
                    f"{self.kind} and priority {self.priority}.",
                    code="user_already_has_picture_with_same_attributes",
                )

    def can_be_seen_by_user(self, request_user):
        photo_visibility = self.user.criuserpreference.photo_visibility
        return (
            request_user.is_authenticated
            and (
                request_user.has_perm("cri_models.view_criuser_photo")
                or photo_visibility >= CRIUserPreference.PhotoVisibility.INTERNAL
                or request_user == self
            )
        ) or photo_visibility == CRIUserPreference.PhotoVisibility.PUBLIC

    @classmethod
    def get_photo_path_dict_for_users_acl(cls, request_user, users):
        photos = (
            cls.objects.filter(user__username__in=users)
            .order_by("user__username", "-kind__priority", "-priority")
            .select_related("user__criuserpreference")
        )
        photos_dict = {}
        for p in photos:
            if photos_dict.get(p.user.username) is not None:
                continue
            photos_dict[p.user.username] = (
                p.image_square.storage.url(p.image_square.name, expire=60)
                if p.can_be_seen_by_user(request_user) and p.image_square.name != ""
                else cls.get_blank_path()
            )
        return photos_dict

    def get_next_priority(self):
        last_photo = (
            CRIPhoto.objects.filter(user=self.user, kind=self.kind)
            .order_by("priority")
            .first()
        )
        if not last_photo:
            return 0
        return last_photo.priority + 1

    def save(self, *args, **kwargs):  # pylint: disable=arguments-differ
        if self.image != self._old_image:
            with self.image.open() as imgfile:
                with Image.open(imgfile) as img:
                    img = ImageOps.exif_transpose(img)
                    self.save_thumb(img)
                    # Compute square image only if it was not provided
                    if (
                        not self._old_image_square and not self.image_square
                    ) or self._old_image_square == self.image_square:
                        self.save_face(img)
                    CRIPhoto.save_image_to_field(img, self.image)
        if self.priority is None:
            self.priority = self.get_next_priority()
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):  # pylint: disable=arguments-differ
        self.image.delete(save=False)
        self.image_thumb.delete(save=False)
        self.image_square.delete(save=False)
        super().delete(*args, **kwargs)

    def __str__(self):
        return f"{self.user.username} ({self.kind}) [{self.priority}]"


class SSHPublicKey(models.Model):
    user = models.ForeignKey(CRIUser, on_delete=models.CASCADE)

    title = models.CharField(blank=True, max_length=128)

    key = fields.SSHPublicKeyField(unique=True)

    key_type = models.CharField(max_length=128, editable=False)

    key_length = models.PositiveIntegerField(editable=False)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "SSH public key"
        ordering = ("user", "title")
        unique_together = (("user", "title"),)

    def save(self, *args, do_sync=True, **kwargs):  # pylint: disable=arguments-differ
        if not hasattr(self.key, "keydata"):
            self.key = self._meta.get_field("key").to_python(self.key)
        if not self.title and self.key.comment:
            self.title = self.key.comment
        self.key_type = self.key.key_type.decode("ascii")
        self.key_length = self.key.bits
        super().save(*args, **kwargs)
        if do_sync:
            transaction.on_commit(CRIUser.delayed_sync(self.user))

    def delete(self, *args, do_sync=True, **kwargs):  # pylint: disable=arguments-differ
        super().delete(*args, **kwargs)
        if do_sync:
            transaction.on_commit(CRIUser.delayed_sync(self.user))


class KerberosPrincipalSyncAdapter(sync.SyncedKerberosPrincipalAdapter):
    def get_sync_remote_obj_id(self, _remote_objects):
        return self.obj.principal

    def get_sync_local_data(self):
        data = {"principal": self.obj.principal}
        if self.obj.user:
            if self.obj.user.is_active:
                data["expire_at"] = None
            else:
                data["expire_at"] = datetime.fromtimestamp(25 * 3600)
        return self.obj.pk, data

    @staticmethod
    def get_sync_remote_data(remote_obj):
        SYNC_FIELDS = ("expire_at",)
        data = {f: getattr(remote_obj, f) for f in SYNC_FIELDS}
        return remote_obj.pk, data


class KerberosPrincipal(sync.SyncedModelMixin, models.Model):
    user = models.ForeignKey(CRIUser, on_delete=models.CASCADE, blank=True, null=True)

    principal = kerberos.fields.KerberosPrincipalField(max_length=100, unique=True)

    out_of_date = models.BooleanField(default=True)

    class SyncMeta:
        sync_adapters = (KerberosPrincipalSyncAdapter,)

    @transaction.atomic
    def save(self, *args, **kwargs):  # pylint: disable=arguments-differ
        super().save(*args, **kwargs)
        # If we moved a principal away from a user for which it was its primary
        # principal, we must also empty the user's primary_principal attribute.
        old_user = CRIUser.objects.filter(primary_principal=self).first()
        if old_user and self.user != old_user:
            old_user.primary_principal = None
            old_user.save()

    def set_and_save_password(self, password, update_out_of_date_status=True):
        principal = kerberos.Principal.get(self.principal)
        principal.set_password(password)
        principal.save()
        if update_out_of_date_status:
            self.out_of_date = False
            self.save()

    def __str__(self):
        return str(self.principal)
