from django.db import models


class MailAlias(models.Model):
    email = models.EmailField(unique=True)

    class Meta:
        ordering = ("email",)
        verbose_name_plural = "mail aliases"

    def __str__(self):
        return self.email
