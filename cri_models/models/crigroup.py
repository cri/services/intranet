from django.db import models, transaction, connection
from django.urls import reverse
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.dispatch import receiver

from cri_models import meili

from .. import ldap, fields, sync


class CRIAbstractMembership(models.Model):
    user = models.ForeignKey("CRIUser", on_delete=models.CASCADE)

    group = models.ForeignKey("CRIGroup", on_delete=models.CASCADE)

    graduation_year = fields.GraduationYearField(blank=True, null=True)

    begin_at = models.DateField(blank=True, null=True)

    end_at = models.DateField(blank=True, null=True)

    class Meta:
        abstract = True

    def __repr__(self):
        return f"<{type(self).__name__}: {self.group}/{self.user}>"

    def is_current(self):
        if self.begin_at and self.begin_at > timezone.now().date():
            return False
        if self.end_at and self.end_at < timezone.now().date():
            return False
        return True

    def clean_fields(self, exclude=None):
        super().clean_fields(exclude)
        if exclude is None or ("end_at" not in exclude and "begin_at" not in exclude):
            if self.end_at and self.begin_at and self.begin_at > self.end_at:
                raise ValidationError(
                    "'end_at' cannot have a value before 'begin_at'.",
                    code="invalid_interval",
                    params={"begin_at": self.begin_at, "end_at": self.end_at},
                )


class CRIMembership(CRIAbstractMembership):
    class Meta:
        verbose_name = "CRI Membership"


@receiver(models.signals.post_save, sender=CRIMembership)
def cri_membership_post_save_handler(sender, **kwargs):
    CRIComputedMembership.compute_entries()


@receiver(models.signals.post_delete, sender=CRIMembership)
def cri_membership_post_delete_handler(sender, **kwargs):
    CRIComputedMembership.compute_entries()


class CRIComputedMembership(CRIAbstractMembership):
    id = models.CharField(primary_key=True)
    user = models.ForeignKey(
        "CRIUser", on_delete=models.DO_NOTHING, related_name="memberships"
    )

    group = models.ForeignKey(
        "CRIGroup", on_delete=models.DO_NOTHING, related_name="memberships"
    )

    class Meta:
        managed = False
        db_table = "cri_models_cricomputedmembership"
        verbose_name = "CRI Computed Membership"

    @classmethod
    def compute_entries(cls):
        with connection.cursor() as cursor:
            cursor.execute("REFRESH MATERIALIZED VIEW cri_models_cricomputedmembership")


class CRICurrentComputedMembership(CRIAbstractMembership):
    id = models.CharField(primary_key=True)
    user = models.ForeignKey(
        "CRIUser", on_delete=models.DO_NOTHING, related_name="current_memberships"
    )

    group = models.ForeignKey(
        "CRIGroup", on_delete=models.DO_NOTHING, related_name="current_memberships"
    )

    class Meta:
        managed = False
        db_table = "cri_models_cricurrentcomputedmembership"
        verbose_name = "CRI Current Computed Memberships"
        verbose_name_plural = "CRI Current Computed Memberships"


class CRIGroupLDAPSyncAdapter(sync.SyncedLDAPModelAdapter):
    sync_remote_model = ldap.LDAPGroup

    def get_sync_remote_obj_id(self, _remote_objects):
        return self.obj.ldap_dn

    def get_sync_local_data(self):
        SYNC_FIELDS = ("gid",)
        data = {
            "ldap_dn": self.obj.ldap_dn,
            "mail_aliases": set(self.obj.get_mail_aliases()),
            "managers": set(u.ldap_dn for u in self.obj.get_managers() if u.ldap_dn),
            "members": set(u.ldap_dn for u in self.obj.get_members() if u.ldap_dn),
            "name": self.obj.slug,
            **{f: getattr(self.obj, f) for f in SYNC_FIELDS},
        }
        return (self.obj.pk, data)

    @staticmethod
    def get_sync_remote_data(remote_obj):
        SYNC_FIELDS = ("name", "gid")
        data = {
            "mail_aliases": set(remote_obj.mail_aliases),
            "managers": set(remote_obj.managers),
            "members": set(remote_obj.members),
            **{f: getattr(remote_obj, f) for f in SYNC_FIELDS},
        }
        return (remote_obj.dn, data)


class CRIGroup(sync.SyncedModelMixin, meili.MeilisearchIndexedMixin, models.Model):
    KIND_CHOICES = (
        ("assistants", "assistants"),
        ("association", "association"),
        ("campus", "campus"),
        ("class", "class"),
        ("course", "course"),
        ("curriculum", "curriculum"),
        ("department", "department"),
        ("laboratory", "laboratory"),
        ("major", "major"),
        ("other", "other"),
        ("program", "program"),
        ("semester", "semester"),
        ("specialization", "specialization"),
        ("system", "system"),
        ("track", "track"),
        ("year", "year"),
    )

    slug = models.SlugField(unique=True)

    gid = fields.UnixIDField(
        unique=True,
        blank=True,
        null=True,
        help_text="Unix group ID",
        verbose_name="GID",
    )

    name = models.CharField(max_length=128)

    kind = models.CharField(max_length=128, choices=KIND_CHOICES, default="class")

    children = models.ManyToManyField(
        "self", blank=True, symmetrical=False, related_name="parents"
    )

    computed_children = models.ManyToManyField(
        "self",
        blank=True,
        editable=False,
        symmetrical=False,
        related_name="computed_parents",
        through="CRIGroupComputedChildren",
    )

    next_group = models.ForeignKey(
        "self",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name="previous_groups",
        help_text=(
            "Group into which to move the current group members at the end of "
            "their membership"
        ),
    )

    managers = models.ManyToManyField(
        "CRIUser",
        blank=True,
        help_text="Users allowed to manage this group",
        related_name="managed_groups",
    )

    private = models.BooleanField(
        default=False, help_text="If true this group will only be shown to its members"
    )

    mail_aliases = models.ManyToManyField(
        "MailAlias",
        blank=True,
        help_text="Mail aliases that can be used by members of this group",
    )

    @property
    def ldap_dn(self):
        if not self.slug:
            raise ValueError(f"Group '{self}' has no slug")
        for adapter in self.get_sync_adapters():
            remote_model = adapter.get_sync_remote_model()
            if issubclass(remote_model, ldap.LDAPModel):
                return remote_model(name=self.slug).build_dn()
        raise ValueError(f"Group '{self}' has no LDAP counterpart")

    class Meta:
        ordering = ("kind", "name", "slug")
        verbose_name = "CRI Group"

    class SyncMeta:
        sync_adapters = (CRIGroupLDAPSyncAdapter,)

    class MeiliMeta(meili.MeilisearchIndexedMixin.MeiliMeta):
        index_name = "groups"
        primary_key = "slug"
        indexed_fields = (
            "slug",
            "name",
            "gid",
            "kind",
            "private",
            "members",
            "type",
            "obj_url",
        )
        filterable_attributes = ("private",)
        searchable_attributes = (
            (
                "slug",
                "name",
                "gid",
            ),
        )

    def __str__(self):
        return self.name

    def members(self):
        if not self.private:
            return []

        members = set(m.pk for m in self.get_members())
        members |= set(m.pk for m in self.get_managers())
        return list(members)

    def get_absolute_url(self):
        return reverse("crigroup_detail", kwargs={"slug": self.slug})

    def get_mail_aliases(self):
        return self.mail_aliases.all().values_list("email", flat=True)

    def get_members(self):
        return (
            get_user_model()
            .objects.filter(
                models.Q(memberships__end_at__gte=timezone.now())
                | models.Q(memberships__end_at__isnull=True),
                models.Q(memberships__begin_at__lte=timezone.now())
                | models.Q(memberships__begin_at__isnull=True),
                memberships__group=self,
            )
            .distinct()
        )

    def get_managers(self):
        return (
            get_user_model()
            .objects.filter(
                models.Q(managed_groups=self)
                | models.Q(managed_groups__computed_children=self)
            )
            .distinct()
        )

    @classmethod
    def get_groups_user_can_see(cls, user):
        if user.has_perm("cri_models.view_crigroup"):
            return cls.objects.all()
        return cls.objects.filter(
            models.Q(memberships__user=user) | models.Q(private=False)
        ).distinct()


class CRIGroupComputedChildren(models.Model):
    id = models.CharField(primary_key=True)
    from_crigroup = models.ForeignKey(CRIGroup, models.DO_NOTHING, related_name="+")
    to_crigroup = models.ForeignKey(CRIGroup, models.DO_NOTHING, related_name="+")

    class Meta:
        managed = False
        db_table = "cri_models_crigroup_computed_children"

    @classmethod
    def refresh_view(cls):
        with connection.cursor() as cursor:
            cursor.execute(
                "REFRESH MATERIALIZED VIEW cri_models_crigroup_computed_children"
            )


@receiver(models.signals.post_delete, sender=CRIGroup)
def delete_group_handler(sender, **kwargs):
    CRIGroupComputedChildren.refresh_view()
    CRIComputedMembership.compute_entries()


@receiver(models.signals.m2m_changed, sender=CRIGroup.children.through)
def update_group_computed_entries(
    sender, instance, action, reverse, model, pk_set, **kwargs
):
    if action in ("post_add", "post_remove", "post_clear"):
        CRIGroupComputedChildren.refresh_view()
        CRIComputedMembership.compute_entries()


class Campus(models.Model):
    group = models.OneToOneField(CRIGroup, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "campuses"
        ordering = ("group__name",)

    def __str__(self):
        return self.group.name

    @property
    def slug(self):
        # Removing this property breaks cri_api.serializers.CampusSerializer.
        return self.group.slug
