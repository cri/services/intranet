from django.conf import settings
import meilisearch
from django.core.paginator import Paginator
import logging

from django.utils.timezone import now
from datetime import timedelta

from . import models

logger = logging.getLogger(__name__)


client = meilisearch.Client(
    settings.MEILISEARCH.get("SERVER_URI"), settings.MEILISEARCH.get("ADMIN_API_KEY")
)
public_key = client.get_key(settings.MEILISEARCH.get("PUBLIC_API_KEY"))


def __get_user_search_filters_for_criuser(user):
    filters = []

    if not user.has_perm("cri_models.view_criuser"):
        filters.append("has_new_account = false")

    if len(filters) == 1:
        return filters[0]

    return " AND ".join(f"({f})" for f in filters)


def __get_user_search_filters_for_crigroup(user):
    filters = []

    if not user.has_perm("cri_models.view_crigroup"):
        filters.append(f"private = false")

    if len(filters) == 1:
        return filters[0]

    return " AND ".join(f"({f})" for f in filters)


def get_user_search_key(user):
    if user.is_anonymous:
        return None

    search_rules = {
        models.CRIUser.MeiliMeta.index_name: {
            "filter": __get_user_search_filters_for_criuser(user),
        },
        models.CRIGroup.MeiliMeta.index_name: {
            "filter": __get_user_search_filters_for_crigroup(user),
        },
    }
    expires_at = now() + timedelta(days=1)

    return client.generate_tenant_token(
        api_key_uid=public_key.uid,
        search_rules=search_rules,
        api_key=public_key.key,
        expires_at=expires_at,
    )


class MeilisearchIndexedMixin:
    class MeiliMeta:
        index_name = None
        indexed_fields = ()
        renamed_fields = ()
        filterable_attributes = ()
        searchable_attributes = ()
        primary_key = "pk"

    def type(self):
        return self._meta.model_name

    def obj_url(self):
        return self.get_absolute_url()

    @classmethod
    def setup_index(cls):
        index = client.index(cls.MeiliMeta.index_name)
        if cls.MeiliMeta.filterable_attributes:
            index.update_filterable_attributes(
                list(cls.MeiliMeta.filterable_attributes)
            )
        # not working for some reasons
        # if cls.MeiliMeta.searchable_attributes:
        #    index.update_searchable_attributes(list(cls.MeiliMeta.searchable_attributes))

    def __get_final_field_name(self, field_name):
        for old_name, final_name in self.MeiliMeta.renamed_fields:
            if old_name == field_name:
                return final_name
        return field_name

    def __get_field(self, field):
        res = getattr(self, field)
        if callable(res):
            res = res()
        primitives = (bool, str, int, float, type(None))
        if isinstance(res, primitives):
            return res
        return res.__str__()

    def __get_model_data(self):
        return {
            self.__get_final_field_name(field): self.__get_field(field)
            for field in self.MeiliMeta.indexed_fields
        }

    def add_to_index(self):
        index = client.index(self.MeiliMeta.index_name)
        data = self.__get_model_data()
        index.add_documents([data], primary_key=self.MeiliMeta.primary_key)

    @classmethod
    def add_all_to_index(cls):
        chunk_size = 1000
        index = client.index(cls.MeiliMeta.index_name)

        qs = cls.objects.all()
        paginator = Paginator(qs, chunk_size)
        for page in paginator:
            logger.info(
                f"Indexing {chunk_size} {cls.__name__} ({page.number}/{paginator.num_pages})"
            )
            documents = list(map(lambda obj: obj.__get_model_data(), page.object_list))
            index.add_documents(
                documents,
                primary_key=cls.MeiliMeta.primary_key,
            )

    def remove_from_index(self):
        index = client.index(self.MeiliMeta.index_name)
        document_id = getattr(self, self.MeiliMeta.primary_key)
        if callable(document_id):
            document_id = document_id()
        index.delete_document(document_id)

    @classmethod
    def remove_all_from_index(cls):
        index = client.index(cls.MeiliMeta.index_name)
        index.delete_all_documents()

    @classmethod
    def delete_index(cls):
        client.delete_index(cls.MeiliMeta.index_name)
