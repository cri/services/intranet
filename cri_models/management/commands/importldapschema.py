from django.core.management.base import BaseCommand

from ... import ldap


class Command(BaseCommand):
    help = "Set up LDAP schema"

    def handle(self, *args, **options):
        self.stdout.write("Importing LDAP schema:")
        orgunits = ("groups", "users", "services")
        for name in orgunits:
            ou, created = ldap.LDAPOrganizationalUnit.objects.get_or_create(name=name)

            if created:
                self.stdout.write(f"  Added {ou.dn}")
            else:
                self.stdout.write(f"  {ou.dn} already exists")
