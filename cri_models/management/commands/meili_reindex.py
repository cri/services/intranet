from django.core.management.base import BaseCommand
from django.apps import apps

from ... import meili


class Command(BaseCommand):
    help = "Add all documents from all indexes"

    def handle(self, *args, **options):
        for model in meili.MeilisearchIndexedMixin.__subclasses__():
            model.add_all_to_index()
