from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from django.conf import settings


class Command(BaseCommand):
    help = "Ensure existence of the initial admin user"

    def handle(self, *args, **options):
        self.stdout.write("Creating or updating the initial admin user.")
        user, _ = get_user_model().objects.update_or_create(
            username=settings.DEFAULT_ADMIN_USERNAME,
            defaults={
                "uid": settings.DEFAULT_ADMIN_UID,
                "last_name": "Administrator",
                "is_staff": True,
                "is_superuser": True,
            },
        )

        for realm in settings.KERBEROS_REALMS.keys():
            principal, _ = user.kerberosprincipal_set.get_or_create(
                principal=f"admin/admin@{realm}"
            )
            if realm == settings.KERBEROS_DEFAULT_REALM:
                user.primary_principal = principal

        user.set_password(settings.DEFAULT_ADMIN_PASSWORD)
        user.save(raise_on_primary_error=False)
        user.primary_group.managers.add(user)
