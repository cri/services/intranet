from django.core.management.base import BaseCommand

from ...kerberos.models import Principal
from ...models import KerberosPrincipal


class Command(BaseCommand):
    help = "Import Kerberos principals"

    def handle(self, *args, **options):
        kerberos_principals = set(p.principal for p in Principal.all())
        db_principals = set(p.principal for p in KerberosPrincipal.objects.all())

        for p in kerberos_principals - db_principals:
            self.stdout.write(f"Importing {p}...")
            KerberosPrincipal.objects.create(principal=p)
