from django.core.management.base import BaseCommand
from django.apps import apps

from ... import meili


class Command(BaseCommand):
    help = "Remove all documents from all indexes"

    def handle(self, *args, **options):
        for model in meili.MeilisearchIndexedMixin.__subclasses__():
            model.delete_index()
