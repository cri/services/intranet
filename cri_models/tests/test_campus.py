from django.test import TestCase

from ..models import CRIGroup, Campus


class TestCampus(TestCase):
    fixtures = ("tests/groups.yaml",)

    def test_str(self):
        TEST_NAME = "test"
        group = CRIGroup(name=TEST_NAME)
        campus = Campus(group=group)
        self.assertEqual(str(campus), TEST_NAME)
