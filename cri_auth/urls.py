from django.urls import include, path

from . import views

urlpatterns = [
    path("", include("social_django.urls", namespace="social")),
    path("login/", views.CRILoginView.as_view(), name="login"),
    path("logout/", views.CRILogoutView.as_view(), name="logout"),
    path(
        "password/change/",
        views.CRIPasswordChangeView.as_view(),
        name="password_change",
    ),
    path(
        "password/recovery/",
        views.CRIPasswordResetView.as_view(),
        name="password_reset",
    ),
    path(
        "password/recovery/confirm/<uidb64>/<token>/",
        views.CRIPasswordResetConfirmView.as_view(),
        name="password_reset_confirm",
    ),
    path("impersonate/", include("cri_auth.impersonate.urls")),
]
