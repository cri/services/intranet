from django.apps import AppConfig


class CRIAuthConfig(AppConfig):
    name = "cri_auth"

    def ready(self):
        # This import statement will trigger checks registration
        from . import checks  # noqa
