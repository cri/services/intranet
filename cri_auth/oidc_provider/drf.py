from rest_framework import authentication as drf_authentication
from rest_framework.exceptions import AuthenticationFailed, PermissionDenied
from oidc_provider import models as oidc_models
from oidc_provider.lib.utils.oauth2 import extract_access_token

import logging

_logger = logging.getLogger(__name__)


class OIDCAccessTokenAuthentication(drf_authentication.BaseAuthentication):
    def authenticate(self, request):
        access_token = extract_access_token(request)
        if not access_token:
            return None

        try:
            token = oidc_models.Token.objects.get(access_token=access_token)
        except oidc_models.Token.DoesNotExist:
            _logger.debug("token %s not found", token)
            raise AuthenticationFailed("invalid_token")

        if token.has_expired():
            _logger.debug("token %s has expired", token)
            raise AuthenticationFailed("expired_token")

        if not token.user.is_auth_allowed():
            _logger.debug("user %s is not allowed", token.user)
            raise AuthenticationFailed("user_is_not_allowed")

        if "api" not in token.scope:
            _logger.debug("token %s is missing the api scope", token)
            raise PermissionDenied("insufficient_scope")

        return (token.user, token)
