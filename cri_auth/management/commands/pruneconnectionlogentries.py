from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone
from datetime import timedelta

from ...models import ConnectionLogEntry


class Command(BaseCommand):
    help = (
        "Delete connection logs entries that are older than the age specified "
        "in settings"
    )

    def handle(self, *args, **options):
        max_age = settings.CONNECTION_LOG_ENTRY_MAX_AGE
        self.stdout.write(f"Deleting log entries older than {max_age} days...")
        oldest_date = timezone.now() - timedelta(days=max_age)
        query = ConnectionLogEntry.objects.filter(created_at__lt=oldest_date)
        res = None
        try:
            res = query.delete()
        except Exception as e:
            raise CommandError(f"ERROR: {e}")
        if res:
            self.stdout.write(self.style.SUCCESS(f"Deleted {res[0]} log entries"))
