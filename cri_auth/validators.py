from django.core.exceptions import ValidationError

import string


class CharacterClassesValidator:
    CLASSES = (
        set(string.ascii_lowercase),
        set(string.ascii_uppercase),
        set(string.digits),
        set(string.punctuation),
    )

    def __init__(self, min_classes=2):
        self.min_classes = min_classes

    def _count_classes(self, password):
        classes = set()
        for char in password:
            for idx, char_class in enumerate(self.CLASSES):
                if char in char_class:
                    classes.add(idx)
        return len(classes)

    def validate(self, password, user=None):
        if self._count_classes(password) < self.min_classes:
            raise ValidationError(
                (
                    "This password must contain at least %(min_classes)d character "
                    "classes."
                ),
                code="password_too_simple",
                params={"min_classes": self.min_classes},
            )

    def get_help_text(self):
        return (
            "Your password must contain at least %(min_classes)d character classes."
            % {"min_classes": self.min_classes}
        )
