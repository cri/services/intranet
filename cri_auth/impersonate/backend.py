from cri_auth.backends import AuthBackendMixin


class ImpersonateBackend(AuthBackendMixin):
    def authenticate(self, request):
        return None
