from django.conf import settings
from django.contrib.auth import login

import logging

_logger = logging.getLogger(__name__)


def stop_impersonation(request):
    if request.real_user.username != request.user.username:
        login(request, request.real_user, settings.AUTHENTICATION_BACKENDS[0])
        _logger.info(
            "Impersonation of '%s' by '%s' has stopped.",
            request.user.username,
            request.real_user.username,
        )


def impersonate(request, user):
    if user == request.real_user:
        stop_impersonation(request)
        return

    login(request, user, "cri_auth.impersonate.backend.ImpersonateBackend")
    request.session["real_user"] = request.real_user.username
    _logger.info(
        "Start impersonation of '%s' by '%s'.",
        request.user.username,
        request.real_user.username,
    )
