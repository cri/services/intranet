from django.utils.deprecation import MiddlewareMixin
from django.contrib.auth import logout, get_user_model

from .views import stop_impersonation

import logging

_logger = logging.getLogger(__name__)


class ImpersonatedRequestMixin:
    user = None

    def __new__(cls, request, *_args, **_kwargs):
        request.__class__ = cls
        return request

    def __init__(self, request, real_user=None):
        self.real_user = real_user

    @property
    def is_impersonated(self):
        return self.real_user != self.user and self.user.is_authenticated


class ImpersonateMiddleware(MiddlewareMixin):
    def process_request(self, request):
        # There are multiple types of requests (HttpRequest, WSGiRequest, etc.)
        # We dynamically define a new class with our mixin and the proper
        # request type as bases.
        request_type = type(
            f"Impersonated{type(request).__name__}",
            (ImpersonatedRequestMixin, type(request)),
            {},
        )
        request = request_type(request, real_user=request.user)

        if not request.user.is_authenticated:
            return self.get_response(request)

        username = request.session.get("real_user")
        if not username:
            return self.get_response(request)

        try:
            request.real_user = get_user_model().objects.get(username=username)
        except get_user_model().DoesNotExist:
            logout(request)
            _logger.error(
                "User '%s' does not exists anymore while impersonating '%s'. "
                "The user has been logged out!",
                username,
                request.user.username,
            )

        if not request.real_user.has_perm("cri_models.impersonate"):
            stop_impersonation(request)
            _logger.info(
                "User '%s' don't have the permission to impersonate "
                "anymore, impersonation has stopped.",
                username,
            )

        return self.get_response(request)
