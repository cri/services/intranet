from django.test import TestCase, Client
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.conf import settings
from django.urls import reverse


class TestImpersonate(TestCase):
    fixtures = ("tests/groups.yaml",)

    def setUp(self):
        super().setUp()
        self.client = Client()

    def tearDown(self):
        super().tearDown()
        self.client.logout()

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.permission = Permission.objects.get(codename="impersonate")
        User = get_user_model()
        user = User(username="test-user-can1", email="test@example.org", uid=11)
        user.save()
        user.user_permissions.add(cls.permission)

        user = User(username="test-user-cannot", email="test@example.org", uid=12)
        user.save()

        user = User(username="test-user-can2", email="test@example.org", uid=13)
        user.save()
        user.user_permissions.add(cls.permission)

        user = User(username="test-user-can3", email="test@example.org", uid=14)
        user.save()
        user.user_permissions.add(cls.permission)
        user = User(username="test-user-can-unsafe1", email="test@example.org", uid=15)
        user.save()
        user.user_permissions.add(cls.permission)
        user = User(username="test-user-can-unsafe2", email="test@example.org", uid=16)
        user.save()
        user.user_permissions.add(cls.permission)

    def assert_not_auth(self, username):
        response = self.client.get("/")
        self.assertNotEqual(response.wsgi_request.user.username, username)

    def assert_auth(self, username):
        response = self.client.get("/")
        self.assertEqual(response.wsgi_request.user.username, username)

    def client_logged(self, username):
        user = get_user_model().objects.get(username=username)
        self.assertNotEqual(user, None)
        self.client.force_login(user)

    def test_no_auth(self):
        response = self.client.post(
            reverse("impersonate", kwargs={"username": "test-user-can1"})
        )
        self.assertEqual(response.status_code, 403)
        self.assert_not_auth("test-user-can1")

    def test_no_perm(self):
        self.client_logged("test-user-cannot")
        response = self.client.post(
            reverse("impersonate", kwargs={"username": "test-user-can1"})
        )
        self.assertEqual(response.status_code, 403)
        self.assert_not_auth("test-user-can1")

    def test_success_self(self):
        self.client_logged("test-user-can1")
        response = self.client.post(
            reverse("impersonate", kwargs={"username": "test-user-can1"})
        )
        self.assertRedirects(response, reverse(settings.LOGIN_REDIRECT_URL))
        self.assert_auth("test-user-can1")

    def test_success(self):
        self.client_logged("test-user-can1")
        response = self.client.post(
            reverse("impersonate", kwargs={"username": "test-user-cannot"})
        )
        self.assertRedirects(response, reverse(settings.LOGIN_REDIRECT_URL))
        self.assert_auth("test-user-cannot")

    def test_delete_impersonator(self):
        self.client_logged("test-user-can-unsafe2")
        self.client.post(reverse("impersonate", kwargs={"username": "test-user-can1"}))
        user = get_user_model().objects.get(username="test-user-can-unsafe2")
        user.delete()
        self.assert_not_auth("test-user-can1")

    def test_double_success(self):
        self.client_logged("test-user-can1")
        self.client.post(reverse("impersonate", kwargs={"username": "test-user-can2"}))
        response = self.client.post(
            reverse("impersonate", kwargs={"username": "test-user-can3"})
        )
        self.assertRedirects(response, reverse(settings.LOGIN_REDIRECT_URL))
        self.assert_auth("test-user-can3")

    def test_double_error(self):
        self.client_logged("test-user-can1")
        self.client.post(
            reverse("impersonate", kwargs={"username": "test-user-cannot"})
        )
        response = self.client.post(
            reverse("impersonate", kwargs={"username": "test-user-can2"})
        )
        self.assertEqual(response.status_code, 403)
        self.assert_auth("test-user-cannot")

    def test_withdraw_perm(self):
        self.client_logged("test-user-can-unsafe1")
        self.client.post(
            reverse("impersonate", kwargs={"username": "test-user-cannot"})
        )
        get_user_model().objects.get(
            username="test-user-can-unsafe1"
        ).user_permissions.remove(self.permission)
        self.assert_auth("test-user-can-unsafe1")

    def test_stop_success(self):
        self.client_logged("test-user-can1")
        self.client.post(
            reverse("impersonate", kwargs={"username": "test-user-cannot"})
        )
        response = self.client.post(reverse("impersonate_stop"))
        self.assertRedirects(response, reverse(settings.LOGIN_REDIRECT_URL))
        self.assert_auth("test-user-can1")

    def test_stop_tricky_loop(self):
        self.client_logged("test-user-can1")
        self.client.post(reverse("impersonate", kwargs={"username": "test-user-can2"}))
        self.client.post(reverse("impersonate", kwargs={"username": "test-user-can3"}))
        self.client.post(reverse("impersonate", kwargs={"username": "test-user-can2"}))
        self.client.post(reverse("impersonate", kwargs={"username": "test-user-can3"}))
        response = self.client.post(reverse("impersonate_stop"))
        self.assertRedirects(response, reverse(settings.LOGIN_REDIRECT_URL))
        self.assert_auth("test-user-can1")
