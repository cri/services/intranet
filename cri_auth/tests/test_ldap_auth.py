from django.http.request import HttpRequest
from django.contrib.auth import get_user_model, authenticate

from cri_models import models, kerberos, ldap
from cri_models.ldap.test import LDAPTestCase


class TestLDAPAuth(LDAPTestCase):
    fixtures = ("tests/groups.yaml",)

    TEST_USER_LOGIN = "test"
    TEST_USER_PASSWORD = "test"  # noqa
    TEST_USER_PRINCIPAL = kerberos.utils.KerberosPrincipalStr(
        primary="test", instance="test"
    )

    @classmethod
    def setUpTestData(cls):
        cls.request = HttpRequest()
        try:
            cls.ldap_user, _created = ldap.LDAPUser.objects.get_or_create(
                login=cls.TEST_USER_LOGIN,
                first_name="test",
                last_name="test",
                full_name="test",
                uid=42,
                gid=42,
                home="/home/test",
                passwords=set((cls.TEST_USER_PASSWORD,)),
            )
        except:  # noqa: E722
            super().tearDownClass()
            raise

        p, _ = kerberos.Principal.get_or_create(cls.TEST_USER_PRINCIPAL)
        try:
            p.set_password(cls.TEST_USER_PASSWORD)
            p.save()

            cls.user = get_user_model()(username=cls.TEST_USER_LOGIN, uid=42)
            cls.user.save()

            cls.krb_principal = models.KerberosPrincipal(
                user=cls.user, principal=cls.TEST_USER_PRINCIPAL
            )
            cls.krb_principal.save()
        except:  # noqa: E722
            cls.tearDownClass()
            raise

    @classmethod
    def tearDownClass(cls):
        try:
            super().tearDownClass()
        finally:
            kerberos.Principal.get(cls.TEST_USER_PRINCIPAL).delete()

    def test_auth_with_db_only(self):
        self.user.primary_principal = self.krb_principal
        self.user.set_password(self.TEST_USER_PASSWORD)
        self.user.ldap_dn = ""
        self.user.save()

        user = authenticate(
            self.request,
            username=self.TEST_USER_LOGIN,
            password=self.TEST_USER_PASSWORD,
        )

        self.assertEqual(user, self.user)

    def test_auth_with_ldap_password(self):
        self.user.set_unusable_password()
        self.user.ldap_dn = self.ldap_user.dn
        self.user.save()

        self.ldap_user.passwords = set((self.TEST_USER_PASSWORD,))
        self.ldap_user.save()

        user = authenticate(
            self.request,
            username=self.TEST_USER_LOGIN,
            password=self.TEST_USER_PASSWORD,
        )

        self.assertEqual(user, self.user)

    def test_auth_with_ldap_sasl(self):
        self.user.set_unusable_password()
        self.user.ldap_dn = self.ldap_user.dn
        self.user.save()
        self.assertFalse(self.user.has_usable_password())

        self.ldap_user.set_sasl(self.TEST_USER_PRINCIPAL)
        self.ldap_user.save()

        p = kerberos.Principal.get(self.TEST_USER_PRINCIPAL)
        p.set_password(self.TEST_USER_PASSWORD)
        p.save()

        user = authenticate(
            self.request,
            username=self.TEST_USER_LOGIN,
            password=self.TEST_USER_PASSWORD,
        )

        self.assertEqual(user, self.user)
        self.assertTrue(user.has_usable_password())
