from django.test import Client
from django.conf import settings
from django.urls import reverse
from django.contrib.auth import get_user_model
from django.core import management

from cri_models.ldap.test import LDAPTestCase

from .. import views
from ..kerberos import gssapi

import subprocess


class TestSPNEGO(LDAPTestCase):
    fixtures = ("tests/groups.yaml",)

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        try:
            subprocess.run(
                [
                    "/usr/bin/kinit",
                    f"admin/admin@{settings.KERBEROS_DEFAULT_REALM}",
                ],
                timeout=10,
                input=settings.DEFAULT_ADMIN_PASSWORD,
                text=True,
                encoding="utf-8",
                stdout=subprocess.DEVNULL,
                check=True,
            )
        except:  # noqa: E722 # pragma: nocover
            super().tearDownClass()
            raise

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        subprocess.run(["/usr/bin/kdestroy"], check=True)

    @classmethod
    def setUpTestData(cls):
        management.call_command("createadminuser")

    def setUp(self):
        super().setUp()
        self.client = Client()
        self.login_page_url = reverse(settings.LOGIN_URL)

    def test_spnego_401_response(self):
        response = self.client.get(self.login_page_url)
        self.assertEqual(response.status_code, 401)

    def test_spnego_www_authenticate_header(self):
        response = self.client.get(self.login_page_url)
        www_authenticate = response.get("WWW-Authenticate", [])
        self.assertIn("Negotiate", www_authenticate)

    def test_no_spnego_response(self):
        spnego_field_name = views.CRILoginView.spnego_field_name
        response = self.client.get(self.login_page_url, data={spnego_field_name: "0"})
        www_authenticate = response.get("WWW-Authenticate", [])
        self.assertEqual(response.status_code, 200)
        self.assertNotIn("Negotiate", www_authenticate)

    def test_no_spnego_when_already_logged_in(self):
        self.client.force_login(get_user_model().objects.get(username="admin"))
        response = self.client.get(self.login_page_url)
        www_authenticate = response.get("WWW-Authenticate", [])
        self.assertEqual(response.status_code, 200)
        self.assertNotIn("Negotiate", www_authenticate)

    def test_spnego_bad_header(self):
        response = self.client.get(self.login_page_url, HTTP_AUTHORIZATION="Negotiate")
        self.assertEqual(response.status_code, 400)
        self.assertIs(response.wsgi_request.user.is_anonymous, True)

    def test_spnego_invalid_auth_data(self):
        auth_data = "INVALID"
        response = self.client.get(
            self.login_page_url, HTTP_AUTHORIZATION=f"Negotiate {auth_data}"
        )
        self.assertEqual(response.status_code, 302)
        www_authenticate = response.get("WWW-Authenticate", [])
        self.assertNotIn("Negotiate", www_authenticate)
        self.assertIs(response.wsgi_request.user.is_anonymous, True)

    def _spnego_auth(self):
        service = f"HTTP@{settings.SPNEGO_HOSTNAME}"
        with gssapi.KerberosGSSAuthClient(service) as krbgssauth:
            _, response = krbgssauth.step("")
            response = self.client.get(
                self.login_page_url, HTTP_AUTHORIZATION=f"Negotiate {response}"
            )
            self.assertEqual(response.status_code, 302)
            self.assertIs(response.wsgi_request.user.is_authenticated, True)
            self.assertRegex(
                response.get("WWW-Authenticate", ""), r"Negotiate\s+\S.*"
            )  # noqa: W605
            for auth_string in response.get("WWW-Authenticate").split(","):
                method, auth_data = auth_string.split(" ", 1)
                if method == "Negotiate":
                    result, _ = krbgssauth.step(auth_data.strip())
                    self.assertEqual(result, krbgssauth.AUTH_GSS_COMPLETE)

    def test_spnego_auth(self):
        self._spnego_auth()

    def test_spnego_auth_is_persistent(self):
        self._spnego_auth()
        response = self.client.get(self.login_page_url)
        self.assertIs(response.wsgi_request.user.is_authenticated, True)
        www_authenticate = response.get("WWW-Authenticate", [])
        self.assertEqual(response.status_code, 200)
        self.assertNotIn("Negotiate", www_authenticate)
