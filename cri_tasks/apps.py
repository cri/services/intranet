from django.apps import AppConfig


class CRITasksConfig(AppConfig):
    name = "cri_tasks"
